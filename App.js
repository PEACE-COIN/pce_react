import './shim.js';
import * as firebase from 'firebase';
import React from 'react';
import * as UiContext from "./src/contexts/ui"
import { Platform, AsyncStorage } from 'react-native';
import Routes from "./src/routes";
import * as Localization from 'expo-localization';
import i18n from 'i18n-js';
import * as Localize from "./src/contexts/localize"
import * as WalletContext from "./src/contexts/wallet"
import * as CurrencyContext from "./src/contexts/currency"
import * as Const from "./src/const/const"

//String.normalizeが動かないので・・
String.prototype.normalize = function (form) { return require('unorm')[String(form).toLowerCase()](this); }

//邪魔なwornigを消す
global.__old_console_warn = global.__old_console_warn || console.warn;
global.console.warn = (...args) => {
  let tst = (args[0] || '') + '';
  if (tst.startsWith('Setting a timer')) {
    return;
  }
  return global.__old_console_warn.apply(console, args);
};

export default function App() {
  //localizationを初期化
  const [locale, setLocale] = React.useState(Localization.locale);
  //console.log("Localization.locale=" + Localization.locale)
  const localizationContext = React.useMemo(
    () => ({
      t: (scope, options) => i18n.t(scope, { locale, ...options }),
      locale,
      setLocale,
    }),
    [locale]
  );

  //UiStateを初期化
  const [applicationState, setApplicationState] = React.useState(UiContext.createApplicationInitalState());

  //walletStateを初期化
  const [currentWalletState, setCurrentWalletState] = React.useState(WalletContext.createCurrentWalletState());

  //currencyStateを初期化
  const [currentCurrencyState, setCurrentCurrencyState] = React.useState(CurrencyContext.createCurrentCurrencyState());

  /**
   * contextからアプリの初期状態を決定、復元する
   */
  React.useEffect(() => {
    //console.log("useEffect run..");

    // Fetch the token from storage then navigate to our appropriate place
    const bootstrapAsync = async () => {
      let user_data;
      let wallet_data;
      let lang_data;
      let current_wallet_data = 0;
      let currency_data = "USD";

      try {
        //await AsyncStorage.removeItem(Const.KEY_USER);
        //ユーザーデータを取得
        user_data = await AsyncStorage.getItem(Const.KEY_USER);
        //ウォレットデータを取得
        wallet_data = await AsyncStorage.getItem(Const.KEY_WALLET);
        //設定言語を取得
        lang_data = await AsyncStorage.getItem(Const.KEY_LANG);
        //選択ウォレットを取得
        current_wallet_data = await AsyncStorage.getItem(Const.KEY_CURRENT_WALLET);
        //通貨データを取得
        currency_data = await AsyncStorage.getItem(Const.KEY_CURRENCY);

      } catch (e) {
        alert("Restoring token failed");
      }

      console.log(user_data);
      console.log(wallet_data);
      console.log("lang_data=" + lang_data);
      console.log("current_wallet_data=" + current_wallet_data);
      console.log("currency_data=" + currency_data);

      //表示ウォレットの初期状態を設定する
      if (current_wallet_data !== null)
        setCurrentWalletState(parseInt(current_wallet_data));
      else
        setCurrentWalletState(0);

      //表示通貨の初期状態を設定する
      if (currency_data === null) {
        if (locale == "ja-JP")
          setCurrentCurrencyState("JPY")
        else
          setCurrentCurrencyState("USD")
      } else {
        setCurrentCurrencyState(currency_data)
      }

      if (user_data == null || user_data === undefined) {
        // Firebaseを初期化
        if (firebase.apps.length === 0) {
          firebase.initializeApp(Const.firebaseConfig);
        }

        //匿名認証エラー処理
        firebase.auth().signInAnonymously().catch(function (error) {
          // Handle Errors here.
          var errorCode = error.code;
          var errorMessage = error.message;

          //console.log(errorCode + ":" + errorMessage)
          alert(errorCode + ":" + errorMessage);
        });

        //firebase匿名認証をする
        firebase.auth().onAuthStateChanged(async (user) => {
          if (user) {
            // User is signed in.
            console.log(user);
            //user
            try {
              await AsyncStorage.setItem(Const.KEY_USER, JSON.stringify(user));
            } catch (error) {
              console.log(error);
            }
            setApplicationState(UiContext.Status.WALLET_NO_EXISTS);
          } else {
            console.log("User is signed out")
          }
        });
      } else if (wallet_data === null || wallet_data === undefined) {
        setApplicationState(UiContext.Status.WALLET_NO_EXISTS);
      } else {
        setApplicationState(UiContext.Status.WALLET_EXISTS);
      }

      //言語設定をする
      if (lang_data == null) {
        try {
          await AsyncStorage.setItem(Const.KEY_LANG, locale);
        } catch (error) {
          console.log("AsyncStorage setLang error..")
          console.log(error);
        }
      } else {
        setLocale(lang_data);
      }

    };

    bootstrapAsync();
  }, [locale]);

  return (
    <Localize.LocalizationContext.Provider value={localizationContext}>
      <UiContext.Context.Provider value={{ applicationState, setApplicationState }}>
        <WalletContext.Context.Provider value={{ currentWalletState, setCurrentWalletState }}>
          <CurrencyContext.Context.Provider value={{ currentCurrencyState, setCurrentCurrencyState }}>
            <Routes />
          </CurrencyContext.Context.Provider>
        </WalletContext.Context.Provider>
      </UiContext.Context.Provider>
    </Localize.LocalizationContext.Provider>
  );
}
