/**
 * localizeContext 
 * 使用する言語の状態を管理する
 */
import * as React from 'react';
import i18n from 'i18n-js';
import {en, ja, pt} from "../const/locale";

i18n.fallbacks = true;
i18n.translations = { ja, en , pt};

export const LocalizationContext = React.createContext();
