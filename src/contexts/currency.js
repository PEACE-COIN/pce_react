/**
 * currencyContext 
 * 初期表示ウォレットの状態を管理する
 */
import * as React from 'react';

/*
 * 初期表示通貨の状態を決定する
*/
export function createCurrentCurrencyState() {
    return "USD";
}

/*
 * 表示通貨の状態変化に必要なプロパティやメソッドをcontextに登録する
*/
export const Context = React.createContext({
    currentCurrencyState: createCurrentCurrencyState(),
    setCurrentCurrencyState: (_) => { },
});