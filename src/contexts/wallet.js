/**
 * walletContext 
 * 初期表示ウォレットの状態を管理する
 */
import * as React from 'react';

/*
 * 初期表示ウォレットの状態を決定する
*/
export function createCurrentWalletState() {
    return 0;
}

/*
 * 表示ウォレットの状態変化に必要なプロパティやメソッドをcontextに登録する
*/
export const Context = React.createContext({
    currentWalletState: createCurrentWalletState(),
    setCurrentWalletState: (_) => { },
});