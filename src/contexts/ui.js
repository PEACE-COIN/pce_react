import React from "react";

/**
 * アプリケーションの状態を定義する
 */
export var Status = {
  UN_AUTHORIZED : "unAuthorized",         //firebase認証されていない。初回起動時。
  WALLET_NO_EXISTS : "walletNoExists",    //ウォレット未作成。TOPからの登録フロー
  REGIST_FINISH : "registfinish",         //ウォレット新規作成完了時。バックアップのナビ等
  RESTORE_FINISH : "restorefinish",       //ウォレット復元完了時。パスコード、指紋認証等
  WALLET_EXISTS : "walletExists",         //ウォレット作成済み。HOME画面へ遷移する
};

/*
 * アプリの初期状態を生成する
*/
export function createApplicationInitalState(){
  return Status.UN_AUTHORIZED;
}

/*
 * 状態変化に必要なプロパティやメソッドをcontextに登録する
*/
export const Context = React.createContext({
    applicationState : createApplicationInitalState(),
    setApplicationState : (_) => {},
});