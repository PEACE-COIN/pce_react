// -------------------------------------------------- 
// 画面定数を定義する
// --------------------------------------------------

/*---------------------------------------------------
 * 画面定数
*/
export const TOP = "TOP";
export const HOME = "HOME";
export const LOADING = "LOADING";
export const ACCOUNT = "ACCOUNT";
export const TOKENHISTORY = "TOKENHISTORY";

export const PASSCODE_INPUT = "PASSCODE_INPUT";
export const PASSCODE_CONFIRM = "PASSCODE_CONFIRM";
export const TOUCHID_INPUT = "TOUCHID_INPUT";
export const MAKE_WALLET_FINISH = "MAKE_WALLET_FINISH";
export const PASS_PHRASE_SHOW = "PASS_PHRASE_SHOW";

export const RESTORE_WALLET = "RESTORE_WALLET";

export const PORICY = 'PORICY';
export const PRIVACY = 'PRIVACY';
export const WALLET_RESET = 'WALLET_RESET';
export const BACKUP = 'BACKUP';
export const SECURITY = 'SECURITY';
export const LANGUAGE = 'LANGUAGE';
export const CURRENCY = 'CURRENCY';

export const CAMERA = 'CAMERA';
export const CURRENCYLIST = 'CURRENCYLIST';

export const MAKEWALLET = 'MAKEWALLET';
export const TRANSACTIONLOG = 'TRANSACTIONLOG';
export const ETHERSCAN = 'ETHERSCAN';

export const RECEIVE_COIN = 'RECEIVE_COIN';
export const SEND_COIN = 'SEND_COIN';
