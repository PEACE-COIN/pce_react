// -------------------------------------------------- 
// 共通の定数を定義する
// --------------------------------------------------
import { Platform } from 'react-native';

/**
 * Storage KEYを定義する
 */
/**
 * ユーザー情報
 * ex) {"uid":"3lYwCcWRE0YBF09wiNHRIRIBMUX2","displayName":null,"photoURL":null,"email":null,"emailVerified":false,"phoneNumber":null,"isAnonymous":true,"tenantId":null,"providerData":[],"apiKey":"AIzaSyBPi1L_sKaQYtFj0kNgfjNCvGAoLTpzdas","appName":"[DEFAULT]","authDomain":"peace-coin-wallet-new.firebaseapp.com","stsTokenManager":{"apiKey":"AIzaSyBPi1L_sKaQYtFj0kNgfjNCvGAoLTpzdas","refreshToken":"AG8BCncb_nVADURtKPxjfzEkUEtiNLNAXEF5f3ASS5rK1gFdt4Tyw8eLtx4as8xLHbmOHgXosZyxXIVkzsBz3EoovD0g8MXvG0AxWhI0y1cGet_nH7ild-XAMGZgm1Qis1GcjH65ZUK3gzBJX0Lbop_uGHId7Q_eABGmT3eRcokylCdRKeIf9f_9j0xe_zK86AtoycRWiwUy","accessToken":"eyJhbGciOiJSUzI1NiIsImtpZCI6IjBlM2FlZWUyYjVjMDhjMGMyODFhNGZmN2RjMmRmOGIyMzgyOGQ1YzYiLCJ0eXAiOiJKV1QifQ.eyJwcm92aWRlcl9pZCI6ImFub255bW91cyIsImlzcyI6Imh0dHBzOi8vc2VjdXJldG9rZW4uZ29vZ2xlLmNvbS9wZWFjZS1jb2luLXdhbGxldC1uZXciLCJhdWQiOiJwZWFjZS1jb2luLXdhbGxldC1uZXciLCJhdXRoX3RpbWUiOjE2MDIyNDI5NDMsInVzZXJfaWQiOiIzbFl3Q2NXUkUwWUJGMDl3aU5IUklSSUJNVVgyIiwic3ViIjoiM2xZd0NjV1JFMFlCRjA5d2lOSFJJUklCTVVYMiIsImlhdCI6MTYwMzAzMjMwOCwiZXhwIjoxNjAzMDM1OTA4LCJmaXJlYmFzZSI6eyJpZGVudGl0aWVzIjp7fSwic2lnbl9pbl9wcm92aWRlciI6ImFub255bW91cyJ9fQ.WazIInH_H9FjbM4w-WfrK3ZknlDXlQtJqsziMN2v9lsOTTRyGaMzp8flMkN78g08hJWVLz8ix4s88K3zNVGzMGUNyURV5bJtR2bEYlv9DTXLaArm_PJibJ4VTRshhWEyQSi3a4Gx7aFIZ9IclJI_Wo_Vv_NylEB4lpindh2B0t52iHtMZ6QCa5qF0eNq6opa3lUzODqUO6JMVApluZGwPL43vgKSuW7avdQSXAx2NooEA_pmLUmOG0Z-kUaSozTtYFaUzKF2m9hGKL2XbL-VdKLjbMhTwtk_XV8lyqYpnrMg6k_8QBh7yyRiXfR76h17VO0JYXd7YYDwWqVvyXM7gw","expirationTime":1603035908000},"redirectEventId":null,"lastLoginAt":"1602242943333","createdAt":"1602242943333","multiFactor":{"enrolledFactors":[]}}
 */
export const KEY_USER = "user";
/**
 * ウォレット情報の配列
 * ex) [{"privateKey":"0x4e6bf2d91fc998078b3d02e80335f8acc031d71cbdb0901baa423b9858287cdf","publicKey":"0x02273639f85f1e74bd809dc3d4914e7e9276c35b2f04a466927b520ece1330605f","parentFingerprint":"0x00000000","fingerprint":"0x2351cd0e","address":"0x1BEC01d7B8B1EBD6685edfEd83837D0c428c7cfb","chainCode":"0xd9ab834324db1ee2aa54d38ca9a0103f0500d0451a92286fc97f84329451701e","index":0,"depth":0,"mnemonic":"とっくん　かほご　たいへん　うんてん　なめる　くつした　ゆうびんきょく　きりん　うこん　かおり　ふおん　てうち　あまやかす　そえもの　いわゆる　ないかく　むすぶ　ぽちぶくろ　みんか　とくべつ　ちゆりょく　のせる　いだく　ごかん","path":"m"}]
 */
export const KEY_WALLET = "wallet";
export const KEY_CURRENT_WALLET = "currentwallet";
export const KEY_TOKENDATA = "tokendata";

//パスコード ex)123456
export const KEY_PASSCODE = "passcode";
//顔、指紋認証　"1":有効 "0":無効
export const KEY_LOCALAUTH = "localauth";
//言語設定
export const KEY_LANG = "language";
//通過設定
export const KEY_CURRENCY = "currency";

/**
 * API KEYを定義する
 */
//coin market cap api key
export const cmc_api_key = "cedb772c-4503-422b-87d5-3d8495c7d48e";
export const cmc_api_url = "https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest";

//LBank api key
export const lbank_api_key = "b2371d78-6218-42f4-aa80-99f1c06c6cf8";
export const lbank_api_url = "https://api.lbkex.com/v2/";

//EtherScan api key
export const etherscan_api_key = "EFMF6G327IJ54EU33DFDBIF7X1J3BINTRC";

//USD->JPYのレート取得
export const rate_api_url = "http://www.gaitameonline.com/rateaj/getrate";

//firebase configuration
export const firebaseConfig = {
    apiKey: "AIzaSyBPi1L_sKaQYtFj0kNgfjNCvGAoLTpzdas",
    authDomain: "peace-coin-wallet-new.firebaseapp.com",
    databaseURL: "https://peace-coin-wallet-new.firebaseio.com",
    projectId: "peace-coin-wallet-new",
    storageBucket: "peace-coin-wallet-new.appspot.com",
    messagingSenderId: "sender-id",
    appId: "1:336539627559:" + Platform.OS + ":94a29451333dcaeed6de02",
    measurementId: "G-measurement-id"
  };

/**
 * その他
 */
//デフォルトのウォレット名
export const DEFAULT_WALLET_NAME = "PEACE COIN Wallet";
//デフォルトの選択TOKEN
export const DEFAULT_SELECTION_TOKEN_KEY = 0;

//ロケールIDの対応言語
export const LANG = [
  {lang:"en", name:"LANG_EN"},
  {lang:"pt", name:"LANG_PT"},
  {lang:"ja-JP", name:"LANG_JP"},
];
//対応通過
export const CURRENCY = [
    {currency:"USD", name:"CURRENCY_USD"},
    {currency:"EUR", name:"CURRENCY_EUR"},
    {currency:"JPY", name:"CURRENCY_JPY"},
];

//扱うトークンの定義
const imagepath = '../../assets/images/token/';
export const token_data = [
  { key: 0, name: 'PEACE COIN', symbol:'PCE',lbankSymbol:'pce_usdt', contract: '0x7c28310CC0b8d898c57b93913098e74a3ba23228', Decimals: 18, balance:"0.0", rate:0,eth_rate:0,icon: require(imagepath + 'icon_peacecoin.png') },
  { key: 1, name: 'PEACE COIN NOTE', symbol:'PCE-NOTE',lbankSymbol:'', contract: '0x6326a02ed7A19Df409b0f63d4b272d11e3dEb14e', Decimals: 18, balance:"0.0", rate:0,eth_rate:0,icon: require(imagepath + 'icon_peacecoinnote.png') },
  { key: 2, name: 'Ethereum', symbol:'ETH', lbankSymbol:'eth_usdt', contract: '', Decimals: 18, balance:"0.0", rate:1, eth_rate:1, icon: require(imagepath + 'icon_ethereum.png') },
]

//ERC20のabi定義
export const erc20abi = [
    // Read-Only Functions
    "function balanceOf(address owner) view returns (uint256)",
    "function decimals() view returns (uint8)",
    "function symbol() view returns (string)",

    // Authenticated Functions
    "function transfer(address to, uint amount) returns (boolean)",

    // Events
    "event Transfer(address indexed from, address indexed to, uint amount)"
];

//接続するネットワーク
export const network = "homestead"

export const blockchainstatus = {"0": "STATUS_NONE" , "1":"STATUS_SUCCESS"};