import { StyleSheet, Text, View, Image, Button, TouchableOpacity } from 'react-native';
/**
 * 共通スタイルを定義する
 */

// ヘッダーのスタイルを定義する
// stack.NavigatorのscreenOptionsで指定されるもの
export const headerstyle = {
    headerTitleAlign: 'center'
};

// スタイルシートの共通で使いそうなスタイルをまとめる
// ex):
// import * as _style from "../../../const/style";
// style={_style.commonstyle.textlink}
export const commonstyle = StyleSheet.create({
    //textのリンク
    textlink: {
        textDecorationLine: "underline",
        color: "#00ABBD",
    },
    subtitletext: {
        marginTop: 30,
        marginBottom: 5,
        marginLeft: 5,
        color: "#808080",
    },
    borderline: {
        borderBottomColor: '#DADADA', 
        borderBottomWidth: 1, 
        marginHorizontal: 10, 
    },
});