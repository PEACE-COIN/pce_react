import React, { useEffect, useRef, useState, useImperativeHandle, forwardRef } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, AsyncStorage, Modal, Animated, Vibration } from 'react-native';
import * as Localize from "../contexts/localize"
import * as LocalAuthentication from 'expo-local-authentication';
import * as Const from '../const/const';
import { Dialog, ProgressDialog, ConfirmDialog } from "react-native-simple-dialogs";
import CustomPinView from './CustomPinView'

/**
 * 指紋、顔認証をする。
 * またはパスコード入力でストレージに格納されているパスコードと一致するか認証する
 * @param {*} props 
 * @param {*} ref 
 */
function LocalAuth(props, ref) {
    //console.log("LocalAuth");

    const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);
    const [authenable, setAuthEnable] = useState(false);
    const [autherror, setAutherror] = useState(0);
    const [modalVisible, setModalVisible] = useState(false);
    const Ref = useRef();
    const [passcode, setPasscode] = useState("");
    const [passcodeerr, setPasscodeErr] = useState("");

    /**
     * touchID認証が使用できるかどうか調べる
     */
    useEffect(() => {
        /**
         * 顔、指紋認証がサポートされている端末かどうかチェックする
         */
        async function supportAuthentication() {
            const compatible = await LocalAuthentication.hasHardwareAsync();
            if (compatible) {
                console.log("有効なデバイスです");
                setAuthEnable(true)
            } else {
                console.log("無効なデバイスです");
                setAuthEnable(false)
            }
            const biometricRecords = await LocalAuthentication.isEnrolledAsync();
            if (biometricRecords) {
                console.log("生体認証有効");
                setAuthEnable(true)
            } else {
                console.log("生体認証無効");
                setAuthEnable(false)
            }
            const types = await LocalAuthentication.supportedAuthenticationTypesAsync()
            console.log(types)

            //顔認証、指紋認証いずれかに対応している場合
            if (types[0] !== "" || types[1] !== "") {
                try {
                    let result = await AsyncStorage.getItem(Const.KEY_LOCALAUTH);
                    if (result === "1" || result === "2") {
                        console.log("TouchID設定有効");
                        setAuthEnable(true)
                    } else {
                        console.log("TouchID設定無効");
                        setAuthEnable(false)
                    }
                } catch (error) {
                    console.log(error);
                }
            }
        }
        supportAuthentication();

    }, [setAuthEnable]
    )

    /**
    * 現在設定してあるパスコードを得る
    */
    useEffect(() => {
        /**
         * 現在設定してあるパスコードを得る
         */
        async function getPincode() {
            console.log("getPincode run..")
            try {
                let passcode = await AsyncStorage.getItem(Const.KEY_PASSCODE);
                setPasscode(passcode);

                console.log(passcode)
            } catch (error) {
                console.log(error);
            }
        }
        getPincode();
    }, [setPasscode]
    )

    /**
    * 認証成功時コールバック
    */
    async function successCallback(enteredPin) {
        console.log(enteredPin);
        if (passcode === enteredPin) {
            //パスコードOKの場合
            setModalVisible(false);
            props.successCallback();
        } else {
            //パスコードが違う場合
            console.log("not match:" + enteredPin + "  " + passcode)
            setPasscodeErr(t("NOT_MATCH_PASSCODE"));
            Ref.current.clearAll();
            Ref.current.shakes();
        }
    }

    /**
     * 指紋、顔認証を実行する
     */
    const handleAuthentication = async () => {
        //指紋、顔認証を実行する
        const result = await LocalAuthentication.authenticateAsync({
            promptMessage: t("USE_TOUCHID_STR"),
            cancelLabel: t("AUTH_CANCEL"),
            fallbackLabel: t("AUTH_FAILED_STR"),
            disableDeviceFallback: false,
        });

        console.log(result)

        if (result.success) {
            //認証成功時は次の画面に遷移する
            props.successCallback();
        } else {
            //認証失敗時はパスコードを入力させる
            LocalAuthentication.cancelAuthenticate();
            setModalVisible(true);
        }
    };

    /**
     * ボタン押下時処理
     */
    async function _doAuthenticate() {
        if (authenable) {
            //認証有効時は認証させる
            await handleAuthentication();
        } else {
            //認証無効時はパスコードを入力させる
            setModalVisible(true);
        }
    }

    /**
     * 外部から関数を呼び出せるようにする
     */
    useImperativeHandle(ref, () => ({
        doAuthenticate() {
            _doAuthenticate();
        },
        callSetAuthEnable() {
            setAuthEnable(!authenable);
        }
    }));

    return (
        <>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert('Modal has been closed.');
                }}>
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>

                        {(passcodeerr === "") ?
                            <Text style={{ color: "#000", marginBottom: 20 }}>{t("PASSCODE_INPUT_STR_1")}</Text>
                            :
                            <Text style={{ color: "#ff0000", marginBottom: 20 }}>{passcodeerr}</Text>
                        }
                        <CustomPinView ref={Ref} successCallback={successCallback} />

                        <TouchableOpacity style={[styles.button2, { marginTop: 20 }]} onPress={() => {
                            setPasscodeErr("")
                            setModalVisible(!modalVisible)
                        }}>
                            <Text style={{ color: '#16a2b3', textAlign: 'center' }}>{t("STR_CANCEL")}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>

            <Dialog
                contentStyle={
                    {
                        alignItems: "center",
                        justifyContent: "center",
                    }
                }
                visible={(autherror === 0) ? false : true}
                title={(autherror === 1) ? t("STR_CANCEL") : t("AUTH_FAILED_TITLE")}
                onTouchOutside={() => setAutherror(0)}
            >
                <View>
                    <Text>{(autherror === 1) ? t("AUTH_CANCEL") : t("AUTH_FAILED_STR")}</Text>
                </View>
            </Dialog>
        </>
    );
}

/**
 * ローカルのスタイルシートを定義する
 */
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: "center"
    },
    button: {
        marginBottom: 20,
        width: 250,
        backgroundColor: '#16a2b3',
        paddingTop: 20,
        paddingBottom: 20,
        borderRadius: 50,
        borderWidth: 1,
        borderColor: '#fff'
    },
    button2: {
        marginBottom: 20,
        width: 250,
        backgroundColor: '#fff',
        paddingTop: 20,
        paddingBottom: 20,
        borderRadius: 50,
        borderWidth: 1,
        borderColor: '#16a2b3'
    },
    text1: {
        fontWeight: 'bold',
        marginTop: 20,
        marginBottom: 20,
        fontSize: 20,
    },
    text2: {
        marginBottom: 20,
        paddingHorizontal: 20,
    },
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 22,
    },
    modalView: {
        margin: 20,
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 35,
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
});

// コンポーネントを forwardRef でラップ
LocalAuth = forwardRef(LocalAuth);
export default LocalAuth;