import React, { useEffect, useRef, useState , forwardRef,useImperativeHandle} from 'react';
import { Animated, Vibration } from 'react-native';
import ReactNativePinView from 'react-native-pin-view';
import Icon from "@expo/vector-icons/Ionicons"

/**
 * パスコード入力機能をサポートする関数コンポーネント
 * 
 * @param {*} props 
 * @param {*} ref 
 */
function CustomPinView(props, ref) {
    const pinView = useRef(null)
    const [showRemoveButton, setShowRemoveButton] = useState(false)
    const [enteredPin, setEnteredPin] = useState("")
    const pin_length = 6;

    useEffect(() => {
        //入力されたコードがあるなら消すボタンを表示
        if (enteredPin.length > 0) {
            setShowRemoveButton(true)
        } else {
            setShowRemoveButton(false)
        }
        //６字入力されているかどうかチェックする
        if (enteredPin.length === pin_length) {
            //OKならコールバックする。使用する各ページにてsuccessCallbackを実装されたい
            props.successCallback(enteredPin);
        }
    }, [enteredPin])

    // シェイクアニメーションへの参照を定義する
    const shake = useRef(new Animated.Value(0)).current;

    // シェイクアニメーション定義
    const shakeAnimation = shake.interpolate({
        inputRange: [0, 0.2, 0.4, 0.6, 0.8, 1],
        outputRange: [0, -20, 20, -20, 20, 0]
    });

    /**
    * pinをシェイクさせる
    */
    const viewShakes = () => {
        // If vibration is enabled then we vibrate on error
        Vibration.vibrate(500);

        // Reset animation to so we can reanimate
        shake.setValue(0);

        // Animate the pins to shake
        Animated.spring(shake, {
            toValue: 1,
            useNativeDriver: false
        }).start();
    }

    /**
     * 外部から関数を呼び出せるようにする
     */
    useImperativeHandle(ref, () => ({
        shakes() {
            viewShakes();
        },
        clearAll(){
            pinView.current.clearAll();
        }
    }));
    
    return (
        <Animated.View style={{ left: shakeAnimation }}>
            <ReactNativePinView
                inputSize={32}
                ref={pinView}
                pinLength={pin_length}
                buttonSize={60}
                onValueChange={value => setEnteredPin(value)}
                buttonAreaStyle={{
                    marginTop: 0,
                    marginHorizontal: 0,
                }}
                inputAreaStyle={{
                    marginBottom: 24,
                }}
                inputViewEmptyStyle={{
                    backgroundColor: "transparent",
                    borderWidth: 1,
                    borderColor: "#16a2b3",
                }}
                inputViewFilledStyle={{
                    backgroundColor: "#16a2b3",
                }}
                buttonViewStyle={{
                    borderWidth: 1,
                }}
                buttonTextStyle={{
                }}
                onButtonPress={key => {
                    if (key === "custom_right") {
                        pinView.current.clear()
                    }
                }}
                customRightButton={showRemoveButton ? <Icon name={"ios-backspace"} size={36} color={"#000"} /> : undefined}
            />
        </Animated.View>
    );
}

// コンポーネントを forwardRef でラップ
CustomPinView = forwardRef(CustomPinView);
export default CustomPinView;