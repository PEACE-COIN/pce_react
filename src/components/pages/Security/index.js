import React, { useEffect, useRef, useState } from 'react';
import { StyleSheet, Text, View, Switch, Dimensions, TouchableOpacity, Image, AsyncStorage } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import * as path from '../../../const/path';
import * as Localize from "../../../contexts/localize"
import * as Const from '../../../const/const';
import LocalAuth from '../../LocalAuth'
import * as Style from '../../../const/style';

export default function SecurityScreen() {
  const win = Dimensions.get('window');
  const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);
  const { navigate } = useNavigation();
  const Ref = useRef();
  var scene = "";

  //トグルスイッチON/OFF
  const [isEnabled, setIsEnabled] = useState(false);
  const [authType, setAuthType] = useState(0);

  /**
   * TOUCHID有効・無効切り替えトグルスイッチのイベントハンドラ
   */
  const toggleSwitch = () => {
    async function setAuthSettings(state) {
      console.log(state)

      if (state) {
        //現在有効の場合、無効にする
        try {
          await AsyncStorage.setItem(Const.KEY_LOCALAUTH, "0");
        } catch (error) {
          console.log(error);
        }
        Ref.current.callSetAuthEnable();
        setIsEnabled(previousState => !previousState)
      } else {
        //現在無効の場合
        scene = "touchid_enable";
        Ref.current.doAuthenticate();
      }

    }
    setAuthSettings(isEnabled);
  };

  /**
   * 現在の認証設定を得る
   */
  useEffect(() => {
    /**
     * 現在の認証設定を得る
     */
    async function getAuthSettings() {
      try {
        let result = await AsyncStorage.getItem(Const.KEY_LOCALAUTH);
        if (result === "1" || result === "2") {
          console.log("TouchID設定有効");
          setIsEnabled(true)
          setAuthType(result);
        } else {
          console.log("TouchID設定無効");
          setIsEnabled(false)
        }
      } catch (error) {
        console.log(error);
      }
    }
    getAuthSettings();

  }, [setIsEnabled])

  /**
   * 認証成功時コールバック
   */
  async function successCallback() {
    console.log("scene=" + scene)
    console.log("authType=" + authType)

    if (scene == "touchid_enable") {
      try {
        await AsyncStorage.setItem(Const.KEY_LOCALAUTH, authType);
      } catch (error) {
        console.log(error);
      }
      Ref.current.callSetAuthEnable();
      setIsEnabled(previousState => !previousState)
    } else if (scene == "passcode_change") {
      navigate(path.PASSCODE_INPUT);
    }
  }

  return (
    <View style={styles.container}>
      <Text style={Style.commonstyle.subtitletext}>{t(path.SECURITY)}</Text>
      <TouchableOpacity style={styles.cell} onPress={() => {
        scene = "passcode_change";
        Ref.current.doAuthenticate();
      }}>
        <Text style={styles.text}>{t("PASSCODE_CHANGE")}</Text>
      </TouchableOpacity>
      <View style={[styles.cell, { flexDirection: 'row' }]}>
        <View style={{ width: (win.width / 2) }}>
          <Text style={styles.text}>{(authType == 1 ? t("TOUCHID_USE") : t("FACEID_USE"))}</Text>
        </View>
        <View style={{ alignItems: "flex-end", paddingRight: 20, width: (win.width / 2) }}>
          <Switch
            trackColor={{ false: '#767577', true: '#81b0ff' }}
            thumbColor={isEnabled ? '#f5dd4b' : '#f4f3f4'}
            ios_backgroundColor="#3e3e3e"
            onValueChange={toggleSwitch}
            value={isEnabled}
          />
        </View>
      </View>
      <LocalAuth ref={Ref} successCallback={successCallback} />
    </View>
  );
}

/**
 * ローカルのスタイルシートを定義する
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  cell: {
    flexDirection: 'row',
    borderStyle: 'solid',
    borderWidth: 0.5,
    borderColor: '#bbb',
    height: 70,
    alignItems: "center",
    backgroundColor: '#fff',
  },
  text: {
    padding: 10,
    fontSize: 16,
    color: "#000"
  }, 
});
