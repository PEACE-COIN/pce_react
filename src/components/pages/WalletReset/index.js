import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, AsyncStorage } from 'react-native';
import Constants from 'expo-constants';
import * as Localize from "../../../contexts/localize"
import { ConfirmDialog } from "react-native-simple-dialogs";
import * as Const from '../../../const/const';
import * as UiContext from "../../../contexts/ui"
import * as WalletContext from "../../../contexts/wallet";

export default function WalletResetScreen() {
  const [confirmvisible, setConfirmVisible] = React.useState(false);
  const uiContext = React.useContext(UiContext.Context);
  const walletContext = React.useContext(WalletContext.Context);

  /**
   * ウォレットをリセットしてTOPに戻る。
   * パスコードと認証設定も初期化してしまう。
   */
  async function wallet_reset() {
    try {
      await AsyncStorage.removeItem(Const.KEY_WALLET);
      await AsyncStorage.removeItem(Const.KEY_PASSCODE);
      await AsyncStorage.removeItem(Const.KEY_LOCALAUTH);
      await AsyncStorage.removeItem(Const.KEY_CURRENT_WALLET);
      //コンテキスト更新
      walletContext.setCurrentWalletState(0);

    } catch (error) {
      console.log("AsyncStorage error..")
      console.log(error);
    }
    uiContext.setApplicationState(UiContext.Status.WALLET_NO_EXISTS);
  }

  const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);
  return (
    <View style={styles.container}>
      <Text style={{ fontWeight: 'bold', marginBottom: 20, fontSize: 20 }}>
        {t("CONFIRM_RESET_WALLET_1")}
      </Text>
      <Text style={{ marginBottom: 20, fontSize: 18, paddingHorizontal: 20 }}>
        {t("CONFIRM_RESET_WALLET_2")}
      </Text>

      <TouchableOpacity style={styles.button} onPress={() => setConfirmVisible(true)}>
        <Text style={{ color: '#fff', textAlign: 'center' }}>{t("WALLET_RESET")}</Text>
      </TouchableOpacity>

      <ConfirmDialog
        title={t("WALLET_RESET")}
        message={t("CONFIRM_RESET_WALLET_1") + "\n" + t("CONFIRM_RESET_WALLET_2")}
        visible={confirmvisible}
        positiveButton={{
          title: "YES",
          onPress: async () => await wallet_reset()
        }}
        negativeButton={{
          title: "NO",
          onPress: () => setConfirmVisible(false)
        }}
      />
    </View>
  );
}

/**
 * ローカルのスタイルシートを定義する
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  button: {
    marginTop: 120,
    width: 250,
    backgroundColor: '#16a2b3',
    paddingTop: 20,
    paddingBottom: 20,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: '#fff'
  },
});