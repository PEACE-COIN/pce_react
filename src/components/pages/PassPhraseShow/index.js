import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, AsyncStorage, Dimensions, FlatList } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import * as path from '../../../const/path';
import * as UiContext from "../../../contexts/ui"
import * as Localize from "../../../contexts/localize"
import * as Const from '../../../const/const';
import * as WalletContext from "../../../contexts/wallet";

const win = Dimensions.get('window');

export default function PassPhraseShowScreen({ route, navigation }) {
  const [wallet, setWallet] = React.useState(null);
  const [address, setAddress] = React.useState(null);

  const { navigate } = useNavigation();
  const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);
  const uiContext = React.useContext(UiContext.Context);
  const walletContext = React.useContext(WalletContext.Context);

  /**
   * ウォレットのニーモニックキーを取得する
   */
  React.useEffect(() => {
    async function getWallet() {
      try {
        var wallet = await AsyncStorage.getItem(Const.KEY_WALLET);
      } catch (error) {
        alert(error);
      }

      let w = JSON.parse(wallet)[walletContext.currentWalletState];

      setAddress(w.address);

      let mnemonic_str = w.mnemonic;
      var array;

      if (mnemonic_str.indexOf('　') > -1) {
        array = mnemonic_str.split('　');
      } else {
        array = mnemonic_str.split(' ');
      }

      console.log(array);
      setWallet(array);
    }
    getWallet();
  }, [setWallet]
  )

  /**
   * flexbox内のセルを定義する
   * @param {*} param0 
   */
  function renderItem({ item, index }) {
    return (
      <View
        style={{
          flex: 1,
          margin: 4,
          minWidth: 170,
          maxWidth: 223,
          height: 20,
          maxHeight: 304,
          //backgroundColor: '#CCC',
          flexDirection: 'row',
        }}
      >
        <Text>{(index + 1)}</Text>
        <Text style={(index < 9) ? { marginLeft: 15 } : { marginLeft: 10 }}>{item}</Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.text1}>{t("KEEP_PASSPHRASES")}</Text>
      <Text style={styles.text2}>{t("STR_TARGET_WALLET_ADDRESS")}:{address}</Text>

      <FlatList
        onEndReachedThreshold={0}
        onEndReached={({ distanceFromEnd }) => {
          console.debug('on end reached ', distanceFromEnd);
        }}
        contentContainerStyle={styles.list}
        data={wallet}
        renderItem={renderItem}
      />

      {(uiContext.applicationState !== UiContext.Status.WALLET_EXISTS) ?
        <TouchableOpacity style={styles.button} onPress={() => uiContext.setApplicationState(UiContext.Status.WALLET_EXISTS)}>
          <Text style={{ color: '#fff', textAlign: 'center' }}>{t("HOME")}</Text>
        </TouchableOpacity>
        :
        <TouchableOpacity style={styles.button} onPress={() => navigate(path.ACCOUNT)}>
          <Text style={{ color: '#fff', textAlign: 'center' }}>{t("STR_BACK")}</Text>
        </TouchableOpacity>
      }
    </View>
  );
}

/**
 * ローカルのスタイルシートを定義する
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  button: {
    marginBottom: 20,
    width: 250,
    backgroundColor: '#16a2b3',
    paddingTop: 20,
    paddingBottom: 20,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: '#fff'
  },
  button2: {
    marginBottom: 20,
    width: 250,
    backgroundColor: '#fff',
    paddingTop: 20,
    paddingBottom: 20,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: '#16a2b3'
  },
  text1: {
    fontWeight: 'bold',
    marginTop: 20,
    marginBottom: 20,
    fontSize: 20,
    paddingHorizontal: 20,
  },
  text2: {
    marginBottom: 20,
    paddingHorizontal: 20,
  },
  list: {
    justifyContent: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
});