import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, AsyncStorage } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import * as path from '../../../const/path';
import * as LocalAuthentication from 'expo-local-authentication';
import Constants from 'expo-constants';
import * as _style from "../../../const/style";
import * as Localize from "../../../contexts/localize"
import * as Const from '../../../const/const';
import { Dialog, ProgressDialog, ConfirmDialog } from "react-native-simple-dialogs";
import { ethers } from 'ethers';
import { ja, en } from 'ethers/wordlists';
import * as UiContext from "../../../contexts/ui"

export default function MakeWalletScreen({ navigation, route }) {
  const uiContext = React.useContext(UiContext.Context);
  const [resultvisible, setResultVisible] = useState(false);
  const [loadingvisible, setLoadingVisible] = useState(false);
  const [walletData, setWalletData] = React.useState(null);
  
  /**
   * ウォレットを得る
   */
  React.useEffect(() => {
    async function getWallet() {
      console.log("getWallet run..");
      try {
        let wallet = await AsyncStorage.getItem(Const.KEY_WALLET);
        console.log(wallet);
        if (wallet !== null)
          setWalletData(JSON.parse(wallet));
      } catch (error) {
        console.log("AsyncStorage error..")
        console.log(error);
      }
    }
    getWallet();
  }, []);

  /*
    * ニーモニックキーからウォレットを生成する。
    * @randomMnemonic ニーモニックキー 
    * @word ワードリスト
    */
  async function makeWallet() {
    console.log("makewallet run..");

    let word = ja;
    if (locale !== "ja-JP")
      word = en;

    console.log(locale);
    //新規アドレス作成
    // Chose the length of your mnemonic:
    //   - 16 bytes => 12 words (* this example)
    //   - 20 bytes => 15 words
    //   - 24 bytes => 18 words
    //   - 28 bytes => 21 words
    //   - 32 bytes => 24 words
    let bytes = ethers.utils.randomBytes(32);
    let randomMnemonic = ethers.utils.HDNode.entropyToMnemonic(bytes, word)
    console.log(randomMnemonic);

    let isValid = ethers.utils.HDNode.isValidMnemonic(randomMnemonic, word)
    console.log(isValid);
    if (isValid) {
      let wallet = null;
      try {
        wallet = ethers.utils.HDNode.fromMnemonic(randomMnemonic, word, null);
        console.log(wallet);
        // ウォレットのアドレスを取得
        let address = wallet.address
        console.log("address:", address)
        // ウォレットのニーモニックを取得
        let mnemonic = wallet.mnemonic
        console.log("mnemonic:", mnemonic)
        // ウォレットの秘密鍵を取得
        let privateKey = wallet.privateKey
        console.log("privateKey:", privateKey)
      } catch (e) {
        console.log("ether wallet make error..")
        console.log(e)
        return;
      }

      wallet.name = Const.DEFAULT_WALLET_NAME;
      wallet.select_token = Const.DEFAULT_SELECTION_TOKEN_KEY;

      try {
        if (walletData !== null) {
          walletData.push(wallet);
          console.log(walletData)
          await AsyncStorage.setItem(Const.KEY_WALLET, JSON.stringify(walletData));
        } else {
          await AsyncStorage.setItem(Const.KEY_WALLET, JSON.stringify([wallet]));
        }
      } catch (error) {
        console.log("AsyncStorage error..")
        console.log(error);
      }

      setLoadingVisible(false);

      navigation.reset({
        index: 0,
        routes: [{ name: path.HOME }],
      })
    }
  }


  const openProgress = () => {
    if (uiContext.applicationState == UiContext.Status.RESTORE_FINISH) {
      //ルーティングを変更
      uiContext.setApplicationState(UiContext.Status.WALLET_EXISTS);
    } else {
      setLoadingVisible(true)

      setTimeout(
        () => {
          makeWallet()
        },
        1000,
      );
    }
  }

  const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);
  return (
    <View style={styles.container}>
      <Text style={{ marginBottom: 40 }}>{t("STR_MAKE_WALLET")}</Text>


      <TouchableOpacity style={styles.button} onPress={async () => {
        openProgress()
      }} >
        <Text style={{ color: '#fff', textAlign: 'center' }}>{t("STR_MAKE")}</Text>
      </TouchableOpacity>

      <Dialog
        contentStyle={
          {
            alignItems: "center",
            justifyContent: "center",
          }
        }
        visible={resultvisible}
        title={t("AUTH_SUCCESS_TITLE")}
      >
        <View style={{ marginBottom: 30 }}>
          <Text>{t("AUTH_SUCCESS_STR")}</Text>
        </View>
        <TouchableOpacity
          onPress={() => {
            setResultVisible(false)
            openProgress()
          }}
          style={styles.button}
        >
          <Text style={{ color: '#fff', textAlign: 'center' }}>OK</Text>
        </TouchableOpacity>
      </Dialog>

      <ProgressDialog
        title={t("WALLET_GENERATE")}
        activityIndicatorColor="blue"
        activityIndicatorSize="large"
        animationType="slide"
        message={t("PLEASE_WAIT")}
        visible={loadingvisible}
      />
    </View>
  );
}

/**
 * ローカルのスタイルシートを定義する
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  button: {
    marginBottom: 20,
    width: 250,
    backgroundColor: '#16a2b3',
    paddingTop: 20,
    paddingBottom: 20,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: '#fff'
  },
  button2: {
    marginBottom: 20,
    width: 250,
    backgroundColor: '#fff',
    paddingTop: 20,
    paddingBottom: 20,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: '#16a2b3'
  },
});