import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Clipboard, Alert } from 'react-native';
import * as Const from "../../../const/const"
import { ethers } from 'ethers';
import * as path from '../../../const/path';
import * as _style from '../../../const/style';
import * as Localize from "../../../contexts/localize"
import moment from 'moment-with-locales-es6';
import { ScrollView } from 'react-native-gesture-handler';
import { AntDesign } from '@expo/vector-icons';
import * as CurrencyContext from "../../../contexts/currency";

export default function TransactionLogScreen({ navigation, route }) {
  const { hash, symbol, amount, address, usdjpyrate, tokenrate, ethrate } = route.params;

  const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);
  const currencyContext = React.useContext(CurrencyContext.Context);
  
  const [receiptData, setReceiptData] = React.useState(null);
  const [tranData, setTranData] = React.useState(null);
  const [blockData, setBlockData] = React.useState(null);

  const [transactionFee, setTransactionFee] = React.useState(0);

  //console.log("ethrate=" + ethrate)
  //console.log("usdjpyrate=" + usdjpyrate)
  //console.log("tokenrate=" + tokenrate)

  React.useEffect(() => {

    moment.locale(locale);
    let etherscanProvider = new ethers.providers.EtherscanProvider(Const.network, Const.etherscan_api_key)

    //Ethereum - ブロック構造
    //https://blog.y-yuki.net/entry/2018/08/13/000000

    etherscanProvider.getTransaction(hash).then((tran) => {
      //console.log(tran);
      setTranData(tran);

      etherscanProvider.getTransactionReceipt(hash).then((receipt) => {
        //console.log(receipt);
        //console.log("cumulativeGasUsed=" + receipt.cumulativeGasUsed);
        setReceiptData(receipt);
        etherscanProvider.getBlock(receipt["blockNumber"]).then((block) => {
          //console.log(block);
          setBlockData(block)

          let transactionFee = parseInt(receipt.gasUsed.toString()) * ethers.utils.formatEther(tran.gasPrice);
          setTransactionFee(transactionFee)
        });
      });
    });
  }, [])

  /**
 * 任意の桁で切り捨てする関数
 * @param {number} value 切り捨てする数値
 * @param {number} base どの桁で切り捨てするか（10→10の位、0.1→小数第１位）
 * @return {number} 切り捨てした値
 */
  function orgFloor(value, n) {
    return Math.floor(value * Math.pow(10, n)) / Math.pow(10, n);
  }

  function onPressStr(value) {
    Clipboard.setString(value);
    Alert.alert(t("STR_DO_COPY"));
  }

  return (
    <ScrollView style={styles.container}>
      {blockData != null && receiptData != null && tranData != null ?
        <>
          <View style={{ paddingVertical: 20, alignItems: "center" }}>
            {address == receiptData.from ?
              <View style={{ flexDirection: "row" }}>
                <AntDesign name="arrowup" size={24} color="blue" />
                <Text style={{ fontSize: 18, paddingLeft: 2 }}>{amount > 0 ? ("-" + amount) : amount} {symbol} ({orgFloor(amount * tokenrate * usdjpyrate, 4)} {currencyContext.currentCurrencyState})</Text>
              </View>
              :
              <View style={{ flexDirection: "row" }}>
                <AntDesign name="arrowdown" size={24} color="red" />
                <Text style={{ fontSize: 18, paddingLeft: 2 }}>{amount > 0 ? ("+" + amount) : amount} {symbol} ({orgFloor(amount * tokenrate * usdjpyrate, 4)} {currencyContext.currentCurrencyState})</Text>
              </View>
            }
          </View>

          <View style={[_style.commonstyle.borderline, {}]}></View>

          <View style={styles.cell}>
            {address == receiptData.from ?
              <>
                <Text style={[styles.text, {}]}>{t("STR_REVEIVER")}</Text>
                <TouchableOpacity onPress={() => onPressStr(receiptData.to)}>
                  <Text style={[styles.text, {}]}>{receiptData.to}</Text>
                </TouchableOpacity>
              </>
              :
              <>
                <Text style={[styles.text, {}]}>{t("STR_SENDER")}</Text>
                <TouchableOpacity onPress={() => onPressStr(receiptData.from)}>
                  <Text style={[styles.text, {}]}>{receiptData.from}</Text>
                </TouchableOpacity>
              </>
            }
          </View>

          <View style={_style.commonstyle.borderline}></View>

          <View style={styles.cell}>
            <Text style={[styles.text, {}]}>{t("STR_TRANSACTION_FEE")}</Text>
            <Text style={[styles.text, {}]}>{transactionFee} ETH({orgFloor(transactionFee * ethrate * usdjpyrate, 4)} {currencyContext.currentCurrencyState})</Text>
          </View>

          <View style={_style.commonstyle.borderline}></View>

          <View style={styles.cell}>
            <Text style={[styles.text, {}]}>{t("STR_STATUS")}</Text>
            <Text style={[styles.text, {}]}>{t(Const.blockchainstatus[receiptData.status])}</Text>
          </View>

          <View style={_style.commonstyle.borderline}></View>

          <View style={styles.cell}>
            <Text style={[styles.text, {}]}>{t("STR_TRANSACTION_HASH")}</Text>
            <TouchableOpacity onPress={() => onPressStr(tranData.hash)}>
              <Text style={[styles.text, {}]}>{tranData.hash}</Text>
            </TouchableOpacity>
          </View>

          <View style={_style.commonstyle.borderline}></View>

          <View style={styles.cell}>
            <Text style={[styles.text, {}]}>{t("STR_TRANSACTION_TIME")}</Text>
            <Text style={[styles.text, {}]}>{moment(new Date(blockData.timestamp * 1000)).format("LLL")}</Text>
          </View>

          <View style={_style.commonstyle.borderline}></View>

          <View style={{ paddingVertical: 20, alignItems: "center" }}>
            <TouchableOpacity style={styles.button} onPress={() => navigation.navigate(path.ETHERSCAN, { hash: tranData.hash })}>
              <Text style={{ color: "#fff" }}>{t("STR_SHOW_ETHERSCAN")}</Text>
            </TouchableOpacity>
          </View>
        </>
        : null}
    </ScrollView>
  );
}

/**
 * ローカルのスタイルシートを定義する
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  text: {
    color: '#707070',
    marginVertical: 2,
  },
  button: {
    marginBottom: 20,
    width: 300,
    backgroundColor: '#FF8F07',
    height: 48,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
    alignItems: "center",
    justifyContent: "center",
  },
  cell: {
    paddingVertical: 15,
    paddingHorizontal: 10
  },
});