import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, Dimensions, KeyboardAvoidingView, Platform, AsyncStorage } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import * as path from '../../../const/path';
import * as UiContext from "../../../contexts/ui"
import * as Localize from "../../../contexts/localize"
import * as Const from '../../../const/const';
import { Dialog, ProgressDialog, ConfirmDialog } from "react-native-simple-dialogs";
import { ethers } from 'ethers';
import { ja, en } from 'ethers/wordlists';
import { RadioButton } from 'react-native-paper';

const win = Dimensions.get('window');

export default function RestoreWalletScreen({ navigation, route }) {
  const pf_ja = "くうぼ　さうな　げいじゅつ　あいこくしん　そげき　みせる　ちたん　おくじょう　れいせい　げつれい　きうん　きかんしゃ　きあつ　なつやすみ　せすじ　しゃれい　にきび　さんこう　てんらんかい　おくる　ずぶぬれ　みもと　あらためる　はなす";
  const pf_en = "badge force scissors raven leave seat fun since spatial upon ask unlock jungle eight claim garlic hundred raven join margin faith gold develop maximum";

  const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);
  const [randomMnemonic, onChangeText] = React.useState('Useless Placeholder');
  const [walletName, onChangeNameText] = React.useState('Useless Placeholder');
  const [loadingvisible, setLoadingVisible] = React.useState(false);
  const [error, setError] = React.useState(null);
  const [lang, setLang] = React.useState('ja');
  const [placeholder, setPlaceholder] = React.useState(pf_en);
  const [walletData, setWalletData] = React.useState(null);
  const uiContext = React.useContext(UiContext.Context);
  //console.log(uiContext.applicationState);

  /**
   * ウォレットを得る
   */
  React.useEffect(() => {
    async function getWallet() {
      console.log("getWallet run.." + locale);
      try {
        let wallet = await AsyncStorage.getItem(Const.KEY_WALLET);
        console.log(wallet);
        if (wallet !== null)
          setWalletData(JSON.parse(wallet));

        if (locale === "ja-JP") {
          setPlaceholder(pf_ja)
          setLang("ja")
        } else {
          setPlaceholder(pf_en)
          setLang("en")
        }

      } catch (error) {
        console.log("AsyncStorage error..")
        console.log(error);
      }
    }
    getWallet();
  }, []);

  /** 
     * ニーモニックキーからウォレットを生成する。
     * @randomMnemonic ニーモニックキー 
     * @word ワードリスト
     */
  async function restoreWallet() {
    console.log("restoreWallet run..");

    let word = ja;
    if (lang !== "ja")
      word = en;

    console.log(randomMnemonic);

    let isValid = ethers.utils.HDNode.isValidMnemonic(randomMnemonic, word)
    console.log(isValid);
    if (isValid) {
      let wallet = null;
      try {
        wallet = ethers.utils.HDNode.fromMnemonic(randomMnemonic, word, null);
        console.log(wallet);
        // ウォレットのアドレスを取得
        let address = wallet.address
        console.log("address:", address)
        // ウォレットのニーモニックを取得
        let mnemonic = wallet.mnemonic
        console.log("mnemonic:", mnemonic)
        // ウォレットの秘密鍵を取得
        let privateKey = wallet.privateKey
        console.log("privateKey:", privateKey)
      } catch (e) {
        console.log("ether wallet make error..")
        console.log(e)
        return;
      }

      //アドレス重複チェック
      let err = 0;
      if (walletData !== null) {
        walletData.forEach((w) => {
          if (w.address == wallet.address) {
            err = 1;
          }
        })
      }

      if (err == 1) {
        setLoadingVisible(false);
        setError(t("ADDRESS_EXISTS"));
      } else {

        wallet.name = Const.DEFAULT_WALLET_NAME;
        wallet.select_token = Const.DEFAULT_SELECTION_TOKEN_KEY;

        try {
          console.log(wallet)
          if (walletData !== null) {
            walletData.push(wallet);
            console.log(walletData)
            await AsyncStorage.setItem(Const.KEY_WALLET, JSON.stringify(walletData));
          } else {
            await AsyncStorage.setItem(Const.KEY_WALLET, JSON.stringify([wallet]));
          }

        } catch (error) {
          console.log("AsyncStorage error..")
          console.log(error);
        }

        setLoadingVisible(false);

        if (uiContext.applicationState === UiContext.Status.WALLET_EXISTS) {
          navigation.reset({
            index: 0,
            routes: [{ name: path.HOME }],
          })
        } else {
          //ルーティングを変更
          uiContext.setApplicationState(UiContext.Status.RESTORE_FINISH)
        }
      }
    } else {
      setLoadingVisible(false);
      setError(t("WRONG_BACKUP_PHRASE"));
    }
  }

  const openProgress = () => {
    setLoadingVisible(true)

    setTimeout(
      () => {
        restoreWallet()
      },
      1000,
    );
  }
  const { navigate } = useNavigation();
  /**
   * チェックが切り替わった時に呼ばれるイベントハンドラ
   * @param {*} language 
   */
  const onChangeCheck = (language) => {
    if (language == "ja")
      setPlaceholder(pf_ja);
    else
      setPlaceholder(pf_en);

    setLang(language)
  }

  return (
    <KeyboardAvoidingView
      style={styles.container}
      keyboardVerticalOffset={Platform.OS == "ios" ? 50 : -30}
      behavior={Platform.OS == "ios" ? "padding" : "height"}>
      <Text style={{ fontWeight: "bold", fontSize: 16, marginBottom: 10, marginHorizontal: 10 }}>
        {t("PLEASE_INPUT_BACKUP_PHRASE")}
      </Text>

      <TextInput
        style={{ width: (win.width * 0.9), height: 120, borderColor: 'gray', borderWidth: 1, marginBottom: 10, textAlignVertical: 'top' }}
        multiline={true}
        onChangeText={text => onChangeText(text)}
        placeholder={placeholder}
      />
      <Text style={{ fontSize: 16, marginBottom: 5, marginHorizontal: 15, textAlign: "auto" }}>
        {t("BACKUP_NOTICE_STR")}
      </Text>
      <View style={{ flexDirection: 'row', }}>
        <RadioButton.Item
          value="en"
          label={t("LANG_EN")}
          status={lang !== 'ja' ? 'checked' : 'unchecked'}
          onPress={() => { onChangeCheck('en') }}
        />
        <RadioButton.Item
          value="ja"
          label={t("LANG_JP")}
          status={lang === 'ja' ? 'checked' : 'unchecked'}
          onPress={() => { onChangeCheck('ja') }}
        />
      </View>
      <Text style={{ fontSize: 16, marginBottom: 15, marginHorizontal: 15, textAlign: "auto" }}>
        {t("BACKUP_PHRASE_NOTICE")}
      </Text>

      <TouchableOpacity style={styles.button} onPress={() => openProgress()}>
        <Text style={{ color: '#fff', textAlign: 'center' }}>{t("IMPORT")}</Text>
      </TouchableOpacity>

      <Dialog
        contentStyle={
          {
            alignItems: "center",
            justifyContent: "center",
          }
        }
        visible={(error === null) ? false : true}
        title={t("STR_ERROR")}
        onTouchOutside={() => setError(null)}
      >
        <View>
          <Text>{error}</Text>
        </View>
      </Dialog>

      <ProgressDialog
        title={t("WALLET_GENERATE")}
        activityIndicatorColor="blue"
        activityIndicatorSize="large"
        animationType="slide"
        message={t("PLEASE_WAIT")}
        visible={loadingvisible}
      />
    </KeyboardAvoidingView>
  );
}

/**
 * ローカルのスタイルシートを定義する
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    marginBottom: 20,
    width: 264,
    backgroundColor: '#16a2b3',
    height: 45,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
    alignItems: "center",
    justifyContent: "center",
  },
});