import React, { useEffect, useRef, useState } from 'react';
import {
  StyleSheet,
  Text,
  SafeAreaView,
  View,
  AsyncStorage,
} from 'react-native';
import * as path from '../../../const/path';
import * as Const from '../../../const/const';
import { useNavigation } from '@react-navigation/native';
import * as Localize from "../../../contexts/localize"
import CustomPinView from '../../CustomPinView'
import * as UiContext from "../../../contexts/ui"

export default function PasscodeConfirmScreen({ route, navigation }) {
  const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);
  const { passcode } = route.params;
  const uiContext = React.useContext(UiContext.Context);
  const { navigate } = useNavigation();
  const [enteredPin, setEnteredPin] = useState("")
  const [codeerror, setCodeerror] = useState(false);
  const Ref = useRef();

  /**
   * 認証成功時コールバック
   */
  async function successCallback(enteredPin) {
    /**
     * ストレージに保存して遷移する
     */
    async function regist(newpasscode) {
      console.log("regist :" + uiContext.applicationState)
      try {
        await AsyncStorage.setItem(Const.KEY_PASSCODE, newpasscode);
      } catch (error) {
        alert(error);
      }

      if (uiContext.applicationState == UiContext.Status.WALLET_NO_EXISTS || uiContext.applicationState == UiContext.Status.RESTORE_FINISH) {
        //新規登録、ウォレットリストアの場合
        navigate(path.TOUCHID_INPUT, {
          passcode: newpasscode
        })
      } else {
        //パスコード変更から来た場合
        navigate(path.ACCOUNT)
      }
    }

    if (passcode === enteredPin) {
      await regist(enteredPin);
    } else {
      setCodeerror(true);
      Ref.current.clearAll();
      Ref.current.shakes();
    }
  }

  return (
    <SafeAreaView
      style={styles.container}>
      <Text style={[styles.instructions,{fontWeight:"bold", marginVertical:30}]}>
        {t("PASSCODE_INPUT_STR_AGAIN")}
      </Text>
      <Text style={styles.instructions}>
        {t("PASSCODE_INPUT_STR_2")}
      </Text>
      {codeerror ? (
        <Text style={{ color: "#ff0000" }}>{t("NOT_MATCH_PASSCODE")}</Text>
      ) : null}
      <View style={styles.pinview}>
        <CustomPinView ref={Ref} successCallback={successCallback} />
      </View>
    </SafeAreaView>
  );
}

/**
 * ローカルのスタイルシートを定義する
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 10,
    marginHorizontal: 30,
  },
  pinview:{
    flex:1,
    marginHorizontal: 50, 
    backgroundColor:"#fff", 
    justifyContent:"center",
  }
});