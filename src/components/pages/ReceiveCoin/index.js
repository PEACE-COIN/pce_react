import React from 'react';
import { StyleSheet, Text, View , TouchableOpacity,Clipboard, Alert,Share} from 'react-native';
import Constants from 'expo-constants';
import * as Localize from "../../../contexts/localize"
import { ConfirmDialog } from "react-native-simple-dialogs";
import * as Const from '../../../const/const';
import * as UiContext from "../../../contexts/ui"
import QRCode from 'react-native-qrcode-svg';

export default function ReceiveCoinScreen({navigation, route}) {
    const {wallet} = route.params;

    const uiContext = React.useContext(UiContext.Context);
    const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);

    const copy = ()=>{
      Clipboard.setString(wallet.address);
      Alert.alert(t("STR_DO_COPY"));
    }

    const onShare = async () => {
      try {
        const result = await Share.share({
          message: wallet.address,
        });
        if (result.action === Share.sharedAction) {
          if (result.activityType) {
            // シェアを中断した場合の処理(iOSのみ)
          } else {
            // シェアを実行した場合(Androidの場合は常にここの処理が実行される)
          }
        } else if (result.action === Share.dismissedAction) {
          // dismissed
        }
      } catch (error) {
        alert(error.message);
      }
    };

    return (
      <View style={styles.container}>
        <View style={{alignItems:"center"}}>
        <Text style={{fontSize:16, fontWeight:"bold"}}>{t("STR_RECVADDRESS")}</Text>
        </View>
        <View style={{alignItems:"flex-start",marginTop:20,marginLeft:10,}}>
          <Text>{wallet.name}</Text>
        </View>
        <View style={{alignItems:"center", marginTop:20,}}>
        <QRCode
          value={wallet.address}
          size={200}
        />
        </View>
        <View style={{alignItems:"center",marginTop:20,}}>
          <Text>{wallet.address}</Text>
        </View>

        <View style={{ flexDirection: "row", justifyContent: "center" ,marginTop:40,}}>
            <TouchableOpacity style={[styles.button, { marginRight: 10 }]}  onPress={copy}>
              <Text style={{ color: '#fff', textAlign: 'center' }}>{t("STR_COPY")}</Text>
            </TouchableOpacity>

            <TouchableOpacity style={[styles.button, { backgroundColor: '#16a2b3', }]} onPress={onShare}>
              <Text style={{ color: '#fff', textAlign: 'center' }}>{t("STR_SHARE")}</Text>
            </TouchableOpacity>
          </View>
      </View>
    );
}

/**
 * ローカルのスタイルシートを定義する
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#fff',

  },
  button: {
    marginBottom: 20,
    width: 147,
    backgroundColor: '#FF8F07',
    height: 38,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
    alignItems: "center",
    justifyContent: "center",
  },
});