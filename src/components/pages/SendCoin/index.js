import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, AsyncStorage, Image, TextInput, Modal, Dimensions, Alert } from 'react-native';
import Constants from 'expo-constants';
import * as Localize from "../../../contexts/localize"
import * as Const from '../../../const/const';
import * as UiContext from "../../../contexts/ui"
import * as WalletContext from "../../../contexts/wallet";
import { ethers } from 'ethers';
import { Dialog, ProgressDialog, ConfirmDialog } from "react-native-simple-dialogs";
import * as path from '../../../const/path';

var printf = require('printf');

const win = Dimensions.get('window');

export default function SendCoinScreen({ navigation, route }) {
  const { address } = route.params;

  const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);
  const uiContext = React.useContext(UiContext.Context);
  const walletContext = React.useContext(WalletContext.Context);

  const [wallet, setWallet] = React.useState(null);
  const [tokenData, setTokenData] = React.useState(null);
  const [amount, setAmount] = React.useState(0);

  const [confirmShow, setConfirmShow] = React.useState(false);
  const [endShow, setEndShow] = React.useState(false);

  const [gasPrice, setGassPrice] = React.useState(0);
  const [loadingvisible, setLoadingVisible] = React.useState(false);

  const [alertmessage, setAlertMessage] = React.useState("");

  const [ethNo, setEthNo] = React.useState();

  React.useEffect(() => {
    (async () => {
      try {
        //ウォレットデータを取得
        let wallet_data = await AsyncStorage.getItem(Const.KEY_WALLET);
        wallet_data = JSON.parse(wallet_data);
        setWallet(wallet_data);
        //tokenデータを取得
        let token_data = await AsyncStorage.getItem(Const.KEY_TOKENDATA);
        token_data = JSON.parse(token_data);
        setTokenData(token_data);

        token_data.forEach(element => {
          if(element["symbol"] == "ETH")
          setEthNo(element["key"]);
        });
      } catch (error) {
        console.log(error);
      }
    })();
  }, []);

  /**
   * 送金金額入力時イベントハンドラ
   * @param {*} value 
   */
  const onChangeAmount = (value) => {
    setAmount(value);
  }

  /**
   * 確認画面のOKボタン押下時処理
   * エラーチェック後、送金を実行する
   */
  const onOkPress = async () => {
    console.log("onOkPress run..")

    let balanceInToken = await getBarance(tokenData[wallet[walletContext.currentWalletState].select_token]);
    //console.log("balanceInToken",parseFloat(balanceInToken))
    if (parseFloat(balanceInToken) < parseFloat(amount)) {
      let error_msg = printf(t("ERR_NO_TOKEN"), tokenData[wallet[walletContext.currentWalletState].select_token]["symbol"]);
      console.log("error_msg", error_msg)
      Alert.alert(error_msg);
      return false;
    }

    // ETH残高を計算
    let balanceInEth = await getBarance(tokenData[ethNo]);

    //ETHがネットワーク手数料を払えるか改めて調べる
    if (balanceInEth < gasPrice) {
      let error_msg = t("ERR_NO_GAS");
      console.log("error_msg", error_msg)
      Alert.alert(error_msg);
      return false;
    }

    setConfirmShow(false);
    setLoadingVisible(true);

    send();

  };

  /*
  * 現在の持ち通貨を得る
  */
  async function getBarance(token_data) {
    console.log("getBarance run..");

    // 接続するノード（INFURA および Etherscan のノードに同時に接続）
    let provider = ethers.getDefaultProvider(Const.network);
    let address = wallet[walletContext.currentWalletState].address;
    let balance = 0;
    let symbol = "ETH";

    if (token_data.contract !== "") {
      let contract = new ethers.Contract(token_data.contract, Const.erc20abi, provider);
      balance = await contract.balanceOf(address);
      symbol = await contract.symbol();
    } else {
      //contractが無い場合はEtherium
      // 渡したアドレスの Ether 残高を取得
      balance = await provider.getBalance(address);
    }

    // 残高を計算
    let balanceInEth = ethers.utils.formatEther(balance)
    console.log(symbol + ":Balance:", balanceInEth)

    return balanceInEth;
  }
  const wait = (timeout) => {
    return new Promise(resolve => {
      setTimeout(resolve, timeout);
    });
  }
  /**
   * 送金を実行する
   */
  async function send() {
    // 接続するノード（INFURA および Etherscan のノードに同時に接続）
    let provider = ethers.getDefaultProvider(Const.network)
    // 秘密鍵
    let privateKey = wallet[walletContext.currentWalletState].privateKey;

    // 秘密鍵からウォレットのインスタンスを作成し、
    // provider をセットします。
    let _wallet = new ethers.Wallet(privateKey, provider)

    //ETHの場合
    if (tokenData[wallet[walletContext.currentWalletState].select_token]["symbol"] == "ETH") {

      // トランザクションオブジェクトを作成
      let tx = {
        to: address,
        // 単位 ether を、単位 wei に変換
        value: ethers.utils.parseEther(amount)
      }

      // トランザクションを送信
      _wallet.sendTransaction(tx)
        .then((txObj) => {
          console.log(txObj);

          wait(2000).then(() => {
            setLoadingVisible(false);
            setEndShow(true);
          });
        })
        .catch(function (e) {
          console.log(e.responseText);

          setLoadingVisible(false);
          let error_msg = JSON.stringify(e.responseText);
          Alert.alert(error_msg);
        })

    } else {
      let contractAddress = tokenData[wallet[walletContext.currentWalletState].select_token].contract;
      var contract = new ethers.Contract(contractAddress, Const.erc20abi, _wallet);

      // How many tokens?
      var numberOfDecimals = tokenData[wallet[walletContext.currentWalletState].select_token].Decimals;
      var numberOfTokens = ethers.utils.parseUnits(amount, numberOfDecimals);

      //console.log("numberOfTokens=" + ethers.utils.formatUnits(numberOfTokens))

      // Send tokens
      contract.transfer(address, numberOfTokens)
        .then(function (txObj) {
          console.log(txObj);

          wait(2000).then(() => {
            setLoadingVisible(false);
            setEndShow(true);
          });
        })
        .catch(function (e) {
          console.log(e.responseText);

          setLoadingVisible(false);
          let error_msg = JSON.stringify(e.responseText);
          Alert.alert(error_msg);
        })
    }
  }

  /**
   * 次へボタン押下時イベントハンドラ
   * 確認画面を表示する
   */
  const onPressNext = async () => {
    let numreg = /^([1-9]\d*|0)(\.\d+)?$/;
    if (numreg.test(amount)) {
      try {
        // 接続するノード（INFURA および Etherscan のノードに同時に接続）
        let provider = ethers.getDefaultProvider(Const.network)
        let _gasPrice = await provider.getGasPrice();

        console.log("gasPrice=" + ethers.utils.formatEther(_gasPrice))

        var transactionObject = {
          from: wallet[walletContext.currentWalletState].address,
          to: address,
          gasPrice: _gasPrice,
        }
        var gasLimit = await provider.estimateGas(transactionObject);

        console.log("gasLimit=" + gasLimit.toString())

        let transactionFee = parseInt(gasLimit.toString()) * ethers.utils.formatEther(_gasPrice);
        console.log(transactionFee)

        setGassPrice(transactionFee);
      } catch (error) {
        console.log(error);
        let error_msg = t("ERR_NO_GAS");
        setAlertMessage(error_msg);
        return false;
      }

      setConfirmShow(true);
    } else {
      let error_msg = t("ERR_INVALIED");
      setAlertMessage(error_msg);
    }
  }

  const onHomeButtonPress = async () => {
    navigation.reset({
      index: 0,
      routes: [{ name: path.HOME }],
    })
  }

  return (
    <View style={styles.container}>
      {wallet != null && tokenData != null ?
        <>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Image style={styles.icon} source={tokenData[wallet[walletContext.currentWalletState].select_token]["icon"]} />
            <View style={{ flexDirection: "column" }}>
              <Text style={{ fontWeight: 'bold', marginTop: 20, fontSize: 20 }}>
                {wallet[walletContext.currentWalletState].name}
              </Text>
              <Text style={{ fontWeight: 'bold', marginBottom: 20, fontSize: 20 }}>
                {tokenData[wallet[walletContext.currentWalletState].select_token]["balance"]} {tokenData[wallet[walletContext.currentWalletState].select_token]["symbol"]}
              </Text>
            </View>
          </View>

          <Text style={{ paddingLeft: 10, color: "#707070" }}>{t("STR_SEND_COST")}</Text>
          <View style={{ flexDirection: "row", alignItems: "center", marginTop: 20, }}>
            <TextInput
              style={{ width: 250, borderColor: 'gray', borderWidth: 1, textAlignVertical: 'top', fontSize: 18, marginLeft: 10, }}
              keyboardType='numeric'
              onChangeText={amoutn => onChangeAmount(amoutn)}
            />
            <Text style={{ fontWeight: 'bold', fontSize: 20 }}>
              {tokenData[wallet[walletContext.currentWalletState].select_token]["symbol"]}
            </Text>
          </View>
          <View style={{ paddingLeft: 10, paddingTop: 10, }}>
            <Text style={{ color: "#707070" }}>{t("STR_SEND_ADDR")}</Text>
            <Text style={{ paddingTop: 10, }}>{address}</Text>
          </View>

          <View style={{ flexDirection: "row", justifyContent: "center", marginTop: 20, }}>
            <TouchableOpacity
              disabled={amount == 0 ? true : false}
              style={[styles.button, { backgroundColor: amount == 0 ? 'gray' : '#16a2b3', }]}
              onPress={onPressNext}>
              <Text style={{ color: '#fff', textAlign: 'center' }}>{t("STR_NEXT")}</Text>
            </TouchableOpacity>
          </View>

          <Modal animationType="slide"
            transparent={true}
            visible={confirmShow}>
            <View style={{ position: "absolute", backgroundColor: "#000", opacity: 0.3, height: win.height, width: win.width }} />
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
              <View style={styles.modalView}>
                <Text style={{ fontSize: 16, fontWeight: "bold" }}>{t("STR_SENDCOIN_CONFIRM")}</Text>

                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Image style={styles.icon} source={tokenData[wallet[walletContext.currentWalletState].select_token]["icon"]} />
                  <View style={{ flexDirection: "column" }}>
                    <Text style={{ fontWeight: 'bold', marginTop: 20, fontSize: 20 }}>
                      {wallet[walletContext.currentWalletState].name}
                    </Text>
                    <Text style={{ fontWeight: 'bold', marginBottom: 20, fontSize: 20 }}>
                      {tokenData[wallet[walletContext.currentWalletState].select_token]["balance"]} {tokenData[wallet[walletContext.currentWalletState].select_token]["symbol"]}
                    </Text>
                  </View>
                </View>

                <View style={{ alignItems: "flex-start", marginTop: 20, width: 300 }}>
                  <Text style={{ paddingLeft: 10, color: "#707070" }}>{t("STR_SEND_COST")}</Text>
                </View>
                <View style={{ alignItems: "flex-start", marginTop: 10, width: 300 }}>
                  <Text style={{ paddingLeft: 10, color: "#707070", fontSize: 20 }}>{amount} {tokenData[wallet[walletContext.currentWalletState].select_token]["symbol"]}</Text>
                </View>
                <View style={{ alignItems: "flex-start", marginTop: 20, width: 300 }}>
                  <Text style={{ paddingLeft: 10, color: "#707070" }}>{t("STR_SEND_GAS")}</Text>
                </View>
                <View style={{ alignItems: "flex-start", marginTop: 10, width: 300 }}>
                  <Text style={{ paddingLeft: 10, color: "#707070", fontSize: 20 }}>{gasPrice} ETH</Text>
                </View>
                <View style={{ alignItems: "flex-start", marginTop: 20, width: 300 }}>
                  <Text style={{ paddingLeft: 10, color: "#707070" }}>{t("STR_SEND_ADDR")}</Text>
                </View>
                <View style={{ alignItems: "flex-start", marginTop: 10, width: 300 }}>
                  <Text style={{ paddingLeft: 10, color: "#707070", fontSize: 20 }}>{address}</Text>
                </View>

                <View style={{ flexDirection: "row", marginTop: 40 }}>
                  <TouchableOpacity style={[styles.button, { marginRight: 10 }]} onPress={() => setConfirmShow(false)}>
                    <Text style={{ color: "#fff" }}>{t("STR_CANCEL")}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.button} onPress={() => onOkPress()}>
                    <Text style={{ color: "#fff" }}>{t("STR_SEND")}</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>

          <Modal animationType="slide"
            transparent={true}
            visible={endShow}>
            <View style={{ position: "absolute", backgroundColor: "#000", opacity: 0.3, height: win.height, width: win.width }} />
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
              <View style={styles.modalView}>
                <Text style={{ fontSize: 16, fontWeight: "bold" , marginBottom: 20}}>{t("STR_SENDCOIN_END")}</Text>

                <Text style={{ fontSize: 16, fontWeight: "bold" }}>{t("STR_SENDCOIN_END_STR")}</Text>
 
                <View style={{ flexDirection: "row", marginTop: 40 }}>
                  <TouchableOpacity style={styles.button} onPress={() => onHomeButtonPress()}>
                    <Text style={{ color: "#fff" }}>{t("HOME")}</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>

          <Dialog
            contentStyle={
              {
                alignItems: "center",
                justifyContent: "center",
              }
            }
            visible={(alertmessage == "") ? false : true}
            onTouchOutside={() => setAlertMessage("")}
          >
            <View>
              <Text>{alertmessage}</Text>
            </View>
          </Dialog>

          <ProgressDialog
            title={t("STR_ON_SEND")}
            activityIndicatorColor="blue"
            activityIndicatorSize="large"
            animationType="none"
            message={t("PLEASE_WAIT")}
            visible={loadingvisible}
          />
        </>
        : null
      }
    </View >
  );
}

/**
 * ローカルのスタイルシートを定義する
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#fff',
  },
  button: {
    marginBottom: 20,
    width: 147,
    backgroundColor: '#FF8F07',
    height: 38,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
    alignItems: "center",
    justifyContent: "center",
  },
  icon: {
    marginLeft: 0,
    width: 100,
    height: 100,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 20,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
});