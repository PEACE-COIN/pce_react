import React, { useEffect, useRef, useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import * as path from '../../../const/path';
import * as Localize from "../../../contexts/localize"
import LocalAuth from '../../LocalAuth'

export default function BackupScreen() {
  const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);

  const { navigate } = useNavigation();
  const Ref = useRef();

  /**
  * 認証成功時コールバック
  */
  function successCallback() {
    navigate(path.PASS_PHRASE_SHOW);
  }

  return (
    <View style={styles.container}>
      <View style={{ flex: 1}}>
        <Text style={[styles.text2, { fontWeight: "bold", marginVertical: 20 }]}>{t("WALLET_CREATE_COMPLETE_STR1")}</Text>
        <Text style={styles.text2}>{t("WALLET_CREATE_COMPLETE_STR2")}</Text>
        <Text style={styles.text2}>{t("WALLET_CREATE_COMPLETE_STR3")}</Text>
        <Text style={styles.text2}>{t("WALLET_CREATE_COMPLETE_STR4")}</Text>
        <Text style={styles.text2}>{t("WALLET_CREATE_COMPLETE_STR5")}</Text>
      </View>

      <TouchableOpacity style={styles.button} onPress={() => Ref.current.doAuthenticate()}>
        <Text style={{ color: '#fff', textAlign: 'center' }}>{t("DO_BACKUP")}</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.button2} onPress={() => navigate(path.ACCOUNT)}>
        <Text style={{ color: '#16a2b3', textAlign: 'center' }}>{t("STR_CANCEL")}</Text>
      </TouchableOpacity>

      <LocalAuth ref={Ref} successCallback={successCallback} />
    </View>
  );
}

/**
 * ローカルのスタイルシートを定義する
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  button: {
    marginBottom: 20,
    width: 250,
    backgroundColor: '#16a2b3',
    paddingTop: 20,
    paddingBottom: 20,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: '#fff'
  },
  button2: {
    marginBottom: 20,
    width: 250,
    backgroundColor: '#fff',
    paddingTop: 20,
    paddingBottom: 20,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: '#16a2b3'
  },
  text1: {
    fontWeight: 'bold',
    marginTop: 20,
    marginBottom: 20,
    fontSize: 20,
  },
  text2: {
    marginBottom: 20,
    paddingHorizontal: 20,
  },
});
