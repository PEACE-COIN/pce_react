import * as React from 'react';
import { WebView } from 'react-native-webview';
import * as Localize from "../../../contexts/localize"

export default function EtherscanScreen({ route }) {
  const { hash } = route.params;

  const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);
  return (
    <WebView
      source={{ uri: "https://etherscan.io/tx/" + hash }}
      style={{ marginTop: 0 }}
    />
  );
}