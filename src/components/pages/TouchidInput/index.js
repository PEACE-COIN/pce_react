import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, AsyncStorage } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import * as path from '../../../const/path';
import * as LocalAuthentication from 'expo-local-authentication';
import Constants from 'expo-constants';
import * as _style from "../../../const/style";
import * as Localize from "../../../contexts/localize"
import * as Const from '../../../const/const';
import { Dialog, ProgressDialog, ConfirmDialog } from "react-native-simple-dialogs";
import { ethers } from 'ethers';
import { ja, en } from 'ethers/wordlists';
import * as UiContext from "../../../contexts/ui"

export default function TouchidInputScreen({ route }) {
  const { passcode } = route.params;
  const uiContext = React.useContext(UiContext.Context);
  const [authenable, setAuthEnable] = useState(false);
  const [authtype, setAuthType] = useState(0);
  const [resultvisible, setResultVisible] = useState(false);
  const [autherror, setAutherror] = useState(0);
  const [loadingvisible, setLoadingVisible] = useState(false);

  /*
    * ニーモニックキーからウォレットを生成する。
    * @randomMnemonic ニーモニックキー 
    * @word ワードリスト
    */
  async function makeWallet() {
    console.log("makewallet run..");

    let word = ja;
    if (locale !== "ja-JP")
      word = en;

    console.log(locale);
    //新規アドレス作成
    // Chose the length of your mnemonic:
    //   - 16 bytes => 12 words (* this example)
    //   - 20 bytes => 15 words
    //   - 24 bytes => 18 words
    //   - 28 bytes => 21 words
    //   - 32 bytes => 24 words
    let bytes = ethers.utils.randomBytes(32);
    let randomMnemonic = ethers.utils.HDNode.entropyToMnemonic(bytes, word)
    console.log(randomMnemonic);

    let isValid = ethers.utils.HDNode.isValidMnemonic(randomMnemonic, word)
    console.log(isValid);
    if (isValid) {
      let wallet = null;
      try {
        wallet = ethers.utils.HDNode.fromMnemonic(randomMnemonic, word, null);
        console.log(wallet);
        // ウォレットのアドレスを取得
        let address = wallet.address
        console.log("address:", address)
        // ウォレットのニーモニックを取得
        let mnemonic = wallet.mnemonic
        console.log("mnemonic:", mnemonic)
        // ウォレットの秘密鍵を取得
        let privateKey = wallet.privateKey
        console.log("privateKey:", privateKey)
      } catch (e) {
        console.log("ether wallet make error..")
        console.log(e)
        return;
      }

      wallet.name = Const.DEFAULT_WALLET_NAME;
      wallet.select_token = Const.DEFAULT_SELECTION_TOKEN_KEY;

      try {
        console.log(JSON.stringify([wallet]))
        await AsyncStorage.setItem(Const.KEY_WALLET, JSON.stringify([wallet]));
      } catch (error) {
        console.log("AsyncStorage error..")
        console.log(error);
      }

      setLoadingVisible(false);

      //ルーティングを変更
      uiContext.setApplicationState(UiContext.Status.REGIST_FINISH);
    }
  }

  useEffect(() => {
    async function supportAuthentication() {
      const compatible = await LocalAuthentication.hasHardwareAsync();
      if (compatible) {
        console.log("有効なデバイスです");
        setAuthEnable(true)
      } else {
        console.log("無効なデバイスです");
        setAuthEnable(false)
      }
      const biometricRecords = await LocalAuthentication.isEnrolledAsync();
      if (biometricRecords) {
        console.log("生体認証有効");
        setAuthEnable(true)
      } else {
        console.log("生体認証無効");
        setAuthEnable(false)
      }
      const types = await LocalAuthentication.supportedAuthenticationTypesAsync();
      types.forEach(type => {
        console.log(type)
        if (type !== "")
          setAuthType(type);
      });
    }
    supportAuthentication();
  }, []
  )

  const handleAuthentication = async () => {
    console.log("authenticateAsync runn..")

    const result = await LocalAuthentication.authenticateAsync({
      promptMessage: (authtype == 1 ? t("USE_TOUCHID_STR") : t("USE_FACEID_STR")),
      cancelLabel: t("AUTH_CANCEL"),
      fallbackLabel: t("AUTH_FAILED_STR"),
      disableDeviceFallback: false,
    });
    console.log(result)

    if (result.success) {
      console.log("authenticateAsync success")
      try {
        await AsyncStorage.setItem(Const.KEY_LOCALAUTH, authtype.toString());
      } catch (error) {
        console.log(error);
      }
      setResultVisible(true)
    } else {
      LocalAuthentication.cancelAuthenticate();
      console.log("authenticateAsync error")
      
      if (result.error === "user_cancel") {
        setAutherror(1)
      } else {
        setAutherror(2)
      }
    }
  };

  const openProgress = () => {
    if (uiContext.applicationState == UiContext.Status.RESTORE_FINISH) {
      //ルーティングを変更
      uiContext.setApplicationState(UiContext.Status.WALLET_EXISTS);
    } else {
      setLoadingVisible(true)

      setTimeout(
        () => {
          makeWallet()
        },
        1000,
      );
    }
  }

  const { navigate } = useNavigation();
  const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);
  const str_enable_touchid = () => {
    switch (authtype) {
      case 1:
        return t("ENABLE_TOUCHID");
      case 2:
        return t("ENABLE_FACEID");
      default:
        return "";
    }
  }
  const str_disable_touchid = () => {
    switch (authtype) {
      case 1:
        return t("DISABLE_TOUCHID");
      case 2:
        return t("DISABLE_FACEID");
      default:
        return "";
    }
  }
  const str_enable_touchstr = () => {
    switch (authtype) {
      case 1:
        return t("ENABLE_TOUCHID_STR");
      case 2:
        return t("ENABLE_FACEID_STR");
      default:
        return "";
    }
  }
  const str_disable_touchstr = () => {
    switch (authtype) {
      case 1:
        return t("DISABLE_TOUCHID_STR");
      case 2:
        return t("DISABLE_FACEID_STR");
      default:
        return "";
    }
  }
  return (
    <View style={styles.container}>
      {authtype == 1 ? (
        <Image style={styles.logo} source={require('../../../../assets/images/touch_id.jpg')} />
      ) : (
          <Image style={styles.logo} source={require('../../../../assets/images/face_id.png')} />
        )}

      <Text style={{ fontWeight: 'bold', marginBottom: 20, fontSize: 20 }}>{authenable ? str_enable_touchid() : str_disable_touchid()}</Text>
      <Text style={{ marginBottom: 40 }}>{authenable ? str_enable_touchstr() : str_disable_touchstr()}</Text>

      {authenable ? (
        <TouchableOpacity style={styles.button} onPress={handleAuthentication} >
          <Text style={{ color: '#fff', textAlign: 'center' }}>{str_enable_touchid()}</Text>
        </TouchableOpacity>
      ) : null}
      <TouchableOpacity style={styles.button2} onPress={async () => {
        openProgress()
      }} >
        <Text style={{ color: '#16a2b3', textAlign: 'center' }}>{t("SKIP")}</Text>
      </TouchableOpacity>

      <Dialog
        contentStyle={
          {
            alignItems: "center",
            justifyContent: "center",
          }
        }
        visible={resultvisible}
        title={t("AUTH_SUCCESS_TITLE")}
      >
        <View style={{ marginBottom: 30 }}>
          <Text>{t("AUTH_SUCCESS_STR")}</Text>
        </View>
        <TouchableOpacity
          onPress={() => {
            setResultVisible(false)
            openProgress()
          }}
          style={styles.button}
        >
          <Text style={{ color: '#fff', textAlign: 'center' }}>OK</Text>
        </TouchableOpacity>
      </Dialog>
      <Dialog
        contentStyle={
          {
            alignItems: "center",
            justifyContent: "center",
          }
        }
        visible={(autherror === 0) ? false : true}
        title={(autherror === 1) ? t("STR_CANCEL") : t("AUTH_FAILED_TITLE")}
        onTouchOutside={() => setAutherror(0)}
      >
        <View>
          <Text>{(autherror === 1) ? t("AUTH_CANCEL") : t("AUTH_FAILED_STR")}</Text>
        </View>
      </Dialog>
      <ProgressDialog
        title={t("WALLET_GENERATE")}
        activityIndicatorColor="blue"
        activityIndicatorSize="large"
        animationType="slide"
        message={t("PLEASE_WAIT")}
        visible={loadingvisible}
      />
    </View>
  );
}

/**
 * ローカルのスタイルシートを定義する
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  button: {
    marginBottom: 20,
    width: 250,
    backgroundColor: '#16a2b3',
    paddingTop: 20,
    paddingBottom: 20,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: '#fff'
  },
  button2: {
    marginBottom: 20,
    width: 250,
    backgroundColor: '#fff',
    paddingTop: 20,
    paddingBottom: 20,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: '#16a2b3'
  },
  logo: {
    marginTop: 30,
    marginBottom: 30,
    resizeMode: "contain",
    height: 150,
  },
});