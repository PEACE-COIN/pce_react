import React from 'react';
import { StyleSheet, Text, SafeAreaView, Image, TouchableOpacity, View, FlatList } from 'react-native';
import * as path from '../../../const/path';
import { useNavigation } from '@react-navigation/native';
import * as Localize from "../../../contexts/localize"
import { Ionicons, FontAwesome,MaterialCommunityIcons } from '@expo/vector-icons';

const iconsize = 32;

export default function AccountScreen() {
  const { navigate } = useNavigation();
  const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);
  const imagepath = '../../../../assets/images/';
  const data = [
    { key: 1, title: 'BACKUP', goto: path.BACKUP, source: require(imagepath + 'accountIconBackup.png') },
    { key: 2, title: 'SECURITY', goto: path.SECURITY, source: require(imagepath + 'accountIconSecurity.png') },
    { key: 3, title: 'LANGUAGE', goto: path.LANGUAGE, source: null, icon: <FontAwesome style={styles.icon}  name="language" size={iconsize}/> },
    { key: 4, title: 'CURRENCY', goto: path.CURRENCY, source: require(imagepath + 'icon-exchange-currency.png') },
    { key: 5, title: 'PRIVACY', goto: path.PRIVACY, source: null, icon: <MaterialCommunityIcons style={styles.icon}  name="file-document-outline" size={iconsize}/> },
    { key: 6, title: 'PORICY', goto: path.PORICY, source: null, icon: <MaterialCommunityIcons style={styles.icon}  name="file-document-outline" size={iconsize}/> },
    { key: 7, title: 'WALLET_RESET', goto: path.WALLET_RESET, source: null, icon:<MaterialCommunityIcons style={styles.icon}  name="credit-card-remove-outline" size={iconsize}/>  },
  ]

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        style={styles.listcontainer}
        data={data}
        removeClippedSubviews={false}
        keyExtractor={data => `${data.key}`}
        renderItem={({ item }) =>
          <>
            <TouchableOpacity style={styles.cell} onPress={() => navigate(item.goto)}>
            {item.source != null ?
              <Image style={styles.image} source={item.source} />
              :
              item.icon
            }
              <Text style={styles.text}>{t(item.title)}</Text>
            </TouchableOpacity>
          </>
        }
      />
    </SafeAreaView>
  );
}

/**
 * ローカルのスタイルシートを定義する
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  listcontainer: {
    marginVertical: 20,
    backgroundColor: '#fff',
  },
  cell: {
    flexDirection: 'row',
    borderStyle: 'solid',
    borderWidth: 0.5,
    borderColor: '#bbb',
    height: 70,
    alignItems: "center",
  },
  text: {
    padding: 10,
    fontSize: 16,
    color: "#000"
  },
  image: {
    marginLeft: 10,
    width: iconsize,
    height: iconsize,
  },
  icon: {
    marginLeft: 10,
    width: iconsize,
    height: iconsize,
    color: "#CFCFCF", 
  },
});