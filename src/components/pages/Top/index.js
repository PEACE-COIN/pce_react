import React from 'react';
import { StyleSheet, Text, View, Image, Button, TouchableOpacity } from 'react-native';
import * as UiContext from "../../../contexts/ui"
import Constants from 'expo-constants';
import { Dimensions } from 'react-native';
import * as Localize from "../../../contexts/localize"
import * as path from '../../../const/path';
import * as Locale from '../../../const/locale';
import { useNavigation } from '@react-navigation/native';
import * as _style from "../../../const/style";

const win = Dimensions.get('window');

export default function TopScreen({ route }) {
  //console.log(route)
  const { navigate } = useNavigation();
  const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);

  return (
    <>
      <View style={styles.container}>
        <Image
          style={styles.logo}
          source={require('../../../../assets/images/wellcomeImgLogo.png')}
        />
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigate(path.PASSCODE_INPUT)}
        >
          <Text style={{ color: '#fff', textAlign: 'center' }}>{t("BTN_NEW_WALLET")}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button2}
          onPress={() => navigate(path.RESTORE_WALLET)}
        >
          <Text style={{ color: '#16a2b3', textAlign: 'center' }}>{t("BTN_RESTORE_WALLET")}</Text>
        </TouchableOpacity>
        <Text style={{fontSize:12}}>{t("REGIST_NOTICE")}</Text>
        <View style={styles.container2}>
          <View style={styles.container3}>
            <TouchableOpacity onPress={() => navigate(path.PORICY)} >
              <Text style={[_style.commonstyle.textlink, {fontSize:12}]}>{t("PORICY")}</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.container3} >
            <TouchableOpacity onPress={() => navigate(path.PRIVACY)} >
              <Text style={[_style.commonstyle.textlink, {fontSize:12}]}>{t("PRIVACY")}</Text>
            </TouchableOpacity>
          </View>
        </View>
        <Image
          style={styles.planet}
          source={require('../../../../assets/images/wellcomeImgPlanet.png')}
        />
      </View>
    </>
  );
}


/**
 * ローカルのスタイルシートを定義する
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Constants.statusBarHeight + 50,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  container2: {
    flex: 2,
    flexDirection: 'row',
  },
  container3: {
    width: 150,
    height: 50,
    alignItems: "center",
    justifyContent: "center"
  },
  button: {
    marginBottom: 16,
    paddingTop: 16,
    paddingBottom: 16,
    width: 264,
    backgroundColor: '#16a2b3',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
    alignItems: "center",
    justifyContent: "center",
  },
  button2: {
    marginBottom: 20,
    paddingTop: 16,
    paddingBottom: 16,
    width: 264,
    backgroundColor: '#fff',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#16a2b3',
    alignItems: "center",
    justifyContent: "center",
  },
  logo: {
    marginTop: 30,
    marginBottom: 30,
    resizeMode: "contain",
    width: 73,
    height: 127,
  },
  planet: {
    position: "absolute",
    bottom: 0,
    width: win.width,
    height: (win.width / (1125 / 420)),
  },
});