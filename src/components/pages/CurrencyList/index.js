import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList, AsyncStorage, Dimensions, TextInput } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import * as Localize from "../../../contexts/localize"
import * as path from '../../../const/path';
import * as UiContext from "../../../contexts/ui";
import * as Const from "../../../const/const"
import { ethers } from 'ethers';
import axios from 'axios';

const win = Dimensions.get('window');

export default function CurrencyListScreen({ navigation, route }) {
  const { currentWallet } = route.params;
  const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);
  //const uiContext = React.useContext(UiContext.Context);
  const [searchtext, onChangeText] = React.useState('');

  const { navigate } = useNavigation();
  const [wallet, setWallet] = React.useState(null);

  const [data, setTokenData] = React.useState(Const.token_data);
  const [data_bk, setTokenDataBk] = React.useState(null);

  const [allWallet, setAllWallet] = React.useState(null);

  /**
  * 現在の認証設定を得る
  */
  React.useEffect(() => {
    console.log("useEffect１ run..")
    /**
     * 現在の認証設定を得る
     */
    async function getWalletSettings() {
      console.log("getWalletSettings run..")
      try {
        //ウォレットデータを取得
        let wallet_data = await AsyncStorage.getItem(Const.KEY_WALLET);

        setWallet(JSON.parse(wallet_data)[currentWallet]);
        setAllWallet(JSON.parse(wallet_data));

      } catch (error) {
        console.log(error);
      }
    }
    getWalletSettings();

    /*
    * LBANKから現在のPCEのETHレートを得る
    * @symbol eth_usdt all 等のキー
    */
    function getPCEPricesByApi(_symbol) {
      const url = Const.lbank_api_url + "ticker.do";

      axios.get(url + "?symbol=" + _symbol)
        .then((results) => {
          console.log(results.data)
          if (results.data.error_code == 0) {
            results.data.data.forEach((tx) => {
              //console.log(tx);

              Const.token_data.forEach((td) => {
                //console.log(td);

                if (td.symbol == "PCE") {
                  let d = data;
                  d[td.key]["eth_rate"] = tx["ticker"]["latest"];
                  setTokenData(d)
                  setTokenDataBk(d)
                }
              })
            })
          }
        }).catch((e) => {
          console.log(e)
        });
    }
    getPCEPricesByApi("pce_eth");
  }, [])

  /**
  * トークンの検索をする
  */
  React.useEffect(() => {
    console.log("searchtext run.. " + searchtext)
    if (data_bk == null)
      return;

    if (searchtext == "") {
      setTokenData(data_bk);
      return;
    }

    var new_data = [];
    data_bk.forEach((tx) => {
      console.log(tx.name.toLowerCase().indexOf(searchtext));
      if (tx.name.toLowerCase().indexOf(searchtext) >= 0) {
        new_data.push(tx);
      }
    })

    setTokenData(new_data)
  }, [searchtext])

  /**
* 現在の通貨のレートを得る
*/
  React.useEffect(() => {
    console.log("useEffect3 run..")
    /*
    * 現在の持ち通貨を得る
    */
    async function getEthBarance(address) {
      console.log("getBalance start..")

      var arrTokenData = data.slice();
      var datagetcount = 0;

      // 接続するノード（INFURA および Etherscan のノードに同時に接続）
      let provider = ethers.getDefaultProvider(Const.network)

      Const.token_data.forEach(async (tx) => {
        if (tx.contract !== "") {
          let contract = new ethers.Contract(tx.contract, Const.erc20abi, provider);
          contract.balanceOf(address)
            .then((balance) => {
              let balanceInEth = ethers.utils.formatEther(balance)
              console.log("Balance:", balanceInEth)

              arrTokenData[tx.key]["balance"] = balanceInEth;

              datagetcount++;

              if (datagetcount == Const.token_data.length) {
                setTokenData(arrTokenData)
                setTokenDataBk(arrTokenData)
                console.log("getBalance end..")
              }
            })

        } else {
          //contractが無い場合はEtherium
          // 渡したアドレスの Ether 残高を取得
          provider.getBalance(address)
            .then((balance) => {
              let balanceInEth = ethers.utils.formatEther(balance)
              console.log("Balance:", balanceInEth)

              arrTokenData[tx.key]["balance"] = balanceInEth;

              datagetcount++;

              if (datagetcount == Const.token_data.length) {
                setTokenData(arrTokenData)
                setTokenDataBk(arrTokenData)
                console.log("getBalance end..")
              }
            })
        }
      })
    }
    //console.log(wallet)
    if (wallet !== null)
      getEthBarance(wallet.address);
  }, [wallet])

  /**
   * HOMEボタンが押された場合のイベントハンドラ
   * @param {*} select_token 
   */
  async function onPress(select_token) {
    allWallet[currentWallet].select_token = select_token;

    try {
      //console.log(JSON.stringify(allWallet))
      await AsyncStorage.setItem(Const.KEY_WALLET, JSON.stringify(allWallet));
    } catch (error) {
      console.log("AsyncStorage error..")
      console.log(error);
    }
    navigation.reset({
      index: 0,
      routes: [{ name: path.HOME }],
    })
  }

  return (
    <View style={styles.container}>
      <TextInput
        style={{ width: (win.width * 0.9), borderColor: 'gray', borderWidth: 1, marginTop: 20, textAlignVertical: 'top', fontSize: 18 }}
        placeholder={t("STR_SEARCH")}
        onChangeText={text => onChangeText(text)}
      />
      <FlatList
        scrollEnabled={true}
        style={styles.listcontainer}
        data={data}
        removeClippedSubviews={false}
        keyExtractor={data => `${data.key}`}
        renderItem={({ item }) =>
          <View style={styles.cell} >
            <View style={{ width: (win.width / 1.20), flexDirection: "row" }}>
              <Image style={styles.icon} source={item.icon} />
              <View style={{ flexDirection: "column", }}>
                <Text style={styles.text_name}>{item.name}</Text>
                <Text style={styles.text}>{item.balance} <Text style={{ fontWeight: "bold" }}>{item.symbol}</Text></Text>
                {(item.eth_rate > 0) ? (
                  <Text style={styles.text_rate}>{item.balance * item.eth_rate} ETH</Text>
                ) : null}
              </View>
            </View>

            {wallet !== null ? (
              <View style={{ alignItems: "flex-end", flexDirection: "row" }}>
                {item.key == wallet.select_token ? (
                  <Image style={[styles.icon, { width: 40, height: 40 }]} source={require('../../../../assets/images/tabbarIconHome.png')} />
                ) : (
                    <TouchableOpacity style={[styles.cell, { height: 40, paddingHorizontal: 10, }]} onPress={() => onPress(item.key)}>
                      <Text style={{}}>{t("STR_SET")}</Text>
                    </TouchableOpacity>
                  )}
              </View>
            ) : null}
          </View>
        }
      />
    </View>
  );
}

/**
 * ローカルのスタイルシートを定義する
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  listcontainer: {
    marginVertical: 20,
    backgroundColor: '#fff',
    width: win.width,
  },
  cell: {
    flexDirection: 'row',
    borderStyle: 'solid',
    borderWidth: 0.5,
    borderColor: '#bbb',
    height: 100,
    alignItems: "center",
  },
  text: {
    fontSize: 16,
    color: "#000",
  },
  text_name: {
    fontSize: 18,
    color: "#000",
    fontWeight: 'bold',
  },
  text_rate: {
    fontSize: 16,
    color: "#CFCFC4",
  },
  icon: {
    marginLeft: 10,
    width: 60,
    height: 60,
  },
});