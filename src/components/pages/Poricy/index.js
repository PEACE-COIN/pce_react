import * as React from 'react';
import { WebView } from 'react-native-webview';
import * as Localize from "../../../contexts/localize"

export default function PoricyScreen() {
      const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);
      return (
        <WebView 
          source={{ uri: (locale === "ja-JP") ? 'https://wallet.a-d.systems/terms_and_conditions_ja.html' : 'https://wallet.a-d.systems/terms_and_conditions_en.html' }} 
          style={{ marginTop: 0 }} 
        />
      );
}