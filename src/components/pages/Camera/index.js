import React, { forwardRef, useImperativeHandle } from "react";
import { Image, TouchableOpacity, StyleSheet, View, Button, Text, Clipboard, Alert, AsyncStorage } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import * as Localize from "../../../contexts/localize"
import * as _style from "../../../const/style";
import { AntDesign } from '@expo/vector-icons';
import { Camera } from 'expo-camera';
import { BarCodeScanner } from 'expo-barcode-scanner';
import { Dimensions } from 'react-native';
import * as path from '../../../const/path';
import coinAddressValidator from 'coin-address-validator';
import * as Const from "../../../const/const"
import * as WalletContext from "../../../contexts/wallet";
import { Dialog, ProgressDialog, ConfirmDialog } from "react-native-simple-dialogs";

const win = Dimensions.get('window');

function CameraScreen({ navigation, route }) {
    const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);
    const walletContext = React.useContext(WalletContext.Context);

    const [hasPermission, setHasPermission] = React.useState(null);
    const [scanned, setScanned] = React.useState(false);
    const Ref = React.createRef();

    const [address, setAddress] = React.useState(null);
    const [alertMessage, setAlertMessage] = React.useState("");

    React.useEffect(() => {
        (async () => {

            //ウォレットデータを取得
            let wallet_data = await AsyncStorage.getItem(Const.KEY_WALLET);
            wallet_data = JSON.parse(wallet_data);
            setAddress(wallet_data[walletContext.currentWalletState].address);

            const { status } = await Camera.requestPermissionsAsync();
            setHasPermission(status === 'granted');
        })();
    }, []);

    /**
     * バーコードからコピーされた時に呼び出される
     */
    const handleBarCodeScanned = ({ type, data }) => {
        if (alertMessage !== "")
            return;

        var addr = data;
        console.log("addr=" + addr + " type=" + type)
        //QRにethereum:が入ってる場合は除く
        if (addr.match(/ethereum:/)) {
            console.log("ethereum:")
            addr = addr.replace(/ethereum:/, "");
        }

        const isEthAddress = coinAddressValidator.validate(addr, 'eth', Const.network);
        if (isEthAddress) {
            if (address !== addr) {
                navigation.navigate(path.SEND_COIN, { address: addr })
            } else {
                setAlertMessage("ERR_NOT_SENDADDRESS");
            }
        } else {
            setAlertMessage("ERR_NOT_ETHADDRESS");
        }
    };

    if (hasPermission === null) {
        return <Text>{t("STR_REQUEST_CAMERA_PARMITTION")}</Text>;
    }
    if (hasPermission === false) {
        return <Text>{t("STR_NO_ACCESS_CAMERA")}</Text>;
    }

    /**
     * クリップボードからコピーされた時に呼び出される
     */
    const copy = async () => {
        const addr = await Clipboard.getString();
        if (addr !== "") {
            const isEthAddress = coinAddressValidator.validate(addr, 'eth', Const.network);
            if (isEthAddress) {
                if (address !== addr) {
                    navigation.navigate(path.SEND_COIN, { address: addr })
                } else {
                    setAlertMessage("ERR_NOT_SENDADDRESS");
                }
            } else {
                setAlertMessage("ERR_NOT_ETHADDRESS");
            }
        } else {
            setAlertMessage("ERR_CLIPBORD_EMPTY");
        }
    }

    return (
        <View style={styles.container}>
            <Camera
                onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
                barCodeScannerSettings={{
                    barCodeTypes: [BarCodeScanner.Constants.BarCodeType.qr],
                }}
                style={{ width: win.width, height: win.width }}>
                <View style={styles.modalView}>
                </View>
            </Camera>
            <View>
                <Text style={{ marginVertical: 20, marginLeft: 10, }}>{t("CAMERA_STR1")}</Text>
            </View>
            {scanned && <Button title={t('CAMERA_STR2')} onPress={() => setScanned(false)} />}

            <TouchableOpacity style={{ alignItems: "center", paddingRight: 10 }} onPress={copy}>
                <Text style={_style.commonstyle.textlink}>{t("STR_PASTE")}</Text>
            </TouchableOpacity>

            <Dialog
                contentStyle={
                    {
                        alignItems: "center",
                        justifyContent: "center",
                    }
                }
                visible={(alertMessage === "") ? false : true}
                title={t("STR_ERROR")}
            >
                <View style={{ marginBottom: 30 }}>
                    <Text>{t(alertMessage)}</Text>
                </View>
                <TouchableOpacity
                    onPress={() => {
                        setAlertMessage("");
                    }}
                    style={styles.button}
                >
                    <Text style={{ color: '#fff', textAlign: 'center' }}>OK</Text>
                </TouchableOpacity>
            </Dialog>
        </View>
    );
}

export default CameraScreen;

/**
 * ローカルのスタイルシートを定義する
 */
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    modalView: {
        backgroundColor: 'transparent',
        borderRadius: 20,
        padding: 35,
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 40,
        height: win.width - 40,
    },
    button: {
        marginBottom: 20,
        width: 147,
        backgroundColor: '#FF8F07',
        height: 38,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#fff',
        alignItems: "center",
        justifyContent: "center",
    },
});