import React from 'react';
import { StyleSheet, Text, View, AsyncStorage, FlatList } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import * as Const from '../../../const/const';
import * as Localize from "../../../contexts/localize"
import { RadioButton } from 'react-native-paper';
import * as path from '../../../const/path';
import * as Style from '../../../const/style';

export default function LanguageScreen() {
  const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);
  const { navigate } = useNavigation();
  const [lang, setLang] = React.useState(locale);

  /**
   * ラジオボタン切り替え時イベントハンドラ
   * @param {*} lang 
   */
  async function onPress(newlang) {
    if(lang != newlang){
      try {
        await AsyncStorage.setItem(Const.KEY_LANG, newlang);
      } catch (error) {
        console.log("AsyncStorage setLang error..")
        console.log(error);
      }
  
      setLocale(newlang);
      navigate(path.ACCOUNT);
    }
  }

  return (
    <View style={styles.container}>
      <Text style={Style.commonstyle.subtitletext}>{t(path.LANGUAGE)}</Text>
      <FlatList
        data={Const.LANG}
        removeClippedSubviews={false}
        keyExtractor={data => `${data.lang}`}
        renderItem={({ item }) =>
          <>
            <RadioButton.Item
              value={item.lang}
              label={t(item.name)}
              status={lang === item.lang ? 'checked' : 'unchecked'}
              onPress={() => { onPress(item.lang) }}
              style={styles.cell}
            />
          </>
        }
      />
    </View>
  );
}

/**
 * ローカルのスタイルシートを定義する
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  cell: {
    flexDirection: 'row',
    borderStyle: 'solid',
    borderWidth: 0.5,
    borderColor: '#bbb',
    height: 70,
    alignItems: "center",
    backgroundColor: '#fff',
  },
  text: {
    padding: 10,
    fontSize: 16,
    color: "#000",
  },
});