import React, { useEffect, useRef, useState } from 'react';
import {
  StyleSheet,
  Text,
  SafeAreaView,
  View,
} from 'react-native';
import * as path from '../../../const/path';
import * as _const from '../../../const/const';
import { useNavigation } from '@react-navigation/native';
import * as Localize from "../../../contexts/localize"
import CustomPinView from '../../CustomPinView'

export default function PasscodeInputScreen({ navigation }) {
  const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);
  const { navigate } = useNavigation();
  const Ref = useRef();

  /**
   * 認証成功時コールバック
   */
  async function successCallback(enteredPin) {
    console.log(enteredPin);
    navigate(path.PASSCODE_CONFIRM, {
      passcode: enteredPin
    })
  }

  return (
    <SafeAreaView
      style={styles.container}>
      <Text style={[styles.instructions,{fontWeight:"bold", marginVertical:30}]}>
        {t("PASSCODE_INPUT_STR_1")}
      </Text>
      <Text style={styles.instructions}>
        {t("PASSCODE_INPUT_STR_2")}
      </Text>
      <View style={styles.pinview}>
        <CustomPinView ref={Ref} successCallback={successCallback} />
      </View>
    </SafeAreaView>
  );
}

/**
 * ローカルのスタイルシートを定義する
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 10,
    marginHorizontal: 30,
  },
  pinview:{
    flex:1,
    marginHorizontal: 50, 
    backgroundColor:"#fff", 
    justifyContent:"center",
  }
});