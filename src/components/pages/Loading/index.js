import React from 'react';
import { StyleSheet, Text, View , ActivityIndicator} from 'react-native';
import Constants from 'expo-constants';

export default function LoadingScreen() {
  state = {
    animating: true
  }

  const animating = state.animating
  
  return (
    <View style={styles.container}>
      <ActivityIndicator
         animating = {animating}
         color = '#0000aa'
         size = "large"
         style = {styles.activityIndicator}/>
    </View>
  );
}

/**
 * ローカルのスタイルシートを定義する
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
  },
});