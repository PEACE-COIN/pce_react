import React from 'react';
import { StyleSheet, Text, View, AsyncStorage, FlatList } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import * as Const from '../../../const/const';
import * as Localize from "../../../contexts/localize"
import { RadioButton } from 'react-native-paper';
import * as path from '../../../const/path';
import * as Style from '../../../const/style';
import * as CurrencyContext from "../../../contexts/currency";

export default function CurrencyScreen() {
  const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);
  const currencyContext = React.useContext(CurrencyContext.Context);
  const { navigate } = useNavigation();

  /**
   * ラジオボタン切り替え時イベントハンドラ
   * @param {*} currency 
   */
  async function onPress(newcurrency) {
    if (currencyContext.currentCurrencyState != newcurrency) {
      try {
        await AsyncStorage.setItem(Const.KEY_CURRENCY, newcurrency);
        currencyContext.setCurrentCurrencyState(newcurrency);
      } catch (error) {
        console.log(error);
      }

      //アカウント画面に戻る
      navigate(path.ACCOUNT);
    }
  }

  return (
    <View style={styles.container}>
      <Text style={Style.commonstyle.subtitletext}>{t(path.CURRENCY)}</Text>
      <FlatList
        data={Const.CURRENCY}
        removeClippedSubviews={false}
        keyExtractor={data => `${data.currency}`}
        renderItem={({ item }) =>
          <>
            <RadioButton.Item
              value={item.currency}
              label={item.currency + " - " + t(item.name)}
              status={currencyContext.currentCurrencyState === item.currency ? 'checked' : 'unchecked'}
              onPress={() => { onPress(item.currency) }}
              style={styles.cell}
            />
          </>
        }
      />
    </View>
  );
}

/**
 * ローカルのスタイルシートを定義する
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  cell: {
    flexDirection: 'row',
    borderStyle: 'solid',
    borderWidth: 0.5,
    borderColor: '#bbb',
    height: 70,
    alignItems: "center",
    backgroundColor: '#fff',
  },
  text: {
    padding: 10,
    fontSize: 16,
    color: "#000",
  },
});