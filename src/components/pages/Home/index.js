import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, AsyncStorage, Dimensions, FlatList, ActivityIndicator, LogBox, Modal, TextInput, ScrollView, RefreshControl } from 'react-native';
import * as Localize from "../../../contexts/localize"
import * as path from '../../../const/path';
import * as _style from '../../../const/style';
import * as WalletContext from "../../../contexts/wallet";
import * as CurrencyContext from "../../../contexts/currency";
import * as Const from "../../../const/const"
import { ethers } from 'ethers';
import axios from 'axios';
import Carousel from 'react-native-snap-carousel';
import moment from 'moment-with-locales-es6';
import { Dialog } from "react-native-simple-dialogs";
import { AntDesign } from '@expo/vector-icons';

const md5Hex = require('md5-hex');
const win = Dimensions.get('window');

export default function HomeScreen({ navigation, route }) {
  //console.log(route)

  const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);
  const walletContext = React.useContext(WalletContext.Context);
  const currencyContext = React.useContext(CurrencyContext.Context);

  const [tokenData, setTokenData] = React.useState(Const.token_data);
  const [historyData, setHistoryData] = React.useState([]);
  const [wallet, setWallet] = React.useState(null);
  const [totalprice, setTotalPrice] = React.useState(0);
  const [rateData, setRateData] = React.useState(null);
  const [rate, setRate] = React.useState(1);
  const [animating, setAnimating] = React.useState(false);
  const [animatingH, setAnimatingH] = React.useState(false);

  const [renameError, setRenameError] = React.useState(false);
  const [renameShow, setRenameShow] = React.useState(false);
  const [walletname, onChangeText] = React.useState('');
  const [refreshing, setRefreshing] = React.useState(false);

  React.useEffect(() => {
    LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
  }, [])

  /**
  * 現在の認証設定を得る
  */
  React.useEffect(() => {
    //console.log("useEffect１ run..")
    /**
     * 現在の認証設定を得る
     */
    async function getWalletSettings() {
      console.log("getWalletSettings run..")

      try {
        //ウォレットデータを取得
        let wallet_data = await AsyncStorage.getItem(Const.KEY_WALLET);
        wallet_data = JSON.parse(wallet_data);
        setWallet(wallet_data);

        console.log("walletContext.currentWalletState=" + walletContext.currentWalletState);
        console.log("currencyContext.currentWalletState=" + currencyContext.currentCurrencyState);

        let addr = wallet_data[walletContext.currentWalletState]["address"];
        //アニメーション開始
        setAnimating(true);
        //現在の持ち通貨を得る
        getBarance(addr);
        //アニメーション開始
        setAnimatingH(true);
        //履歴を得る
        getHistory(addr, wallet_data[walletContext.currentWalletState].select_token);

      } catch (error) {
        console.log(error);
      }

      //USD->JPYレート取得
      axios.get(Const.rate_api_url, null)
        .then((results) => {
          rate_set(results["data"]["quotes"])
          setRateData(results["data"]["quotes"]);
        })
        .catch((error) => {
          console.log(error);
        });

      //lbankから通貨のusdtレートを得る
      const url = Const.lbank_api_url + "ticker.do";

      axios.get(url + "?symbol=all")
        .then((results) => {
          //console.log(results.data)
          let data = results.data;
          data.data.forEach((tx) => {
            //console.log(tx);

            Const.token_data.forEach((td) => {
              if (tx["symbol"] == td.lbankSymbol) {
                //console.log(tx)
                //レートをtokenDataに格納する
                //配列の参照渡しのためuseEffectは走らないことに注意
                let data = tokenData;
                data[td.key]["rate"] = tx["ticker"]["latest"];
                setTokenData(data);
              }
            })
          })
          //console.log(tokenData)
        }).catch((e) => {
          console.log(e)
        });
    }

    getWalletSettings();
  }, [])


  /**
   * tokenDataからtotalを得る
   */
  React.useEffect(() => {
    console.log("tokenData useEffect run..")

    //console.log(rate)

    var total = 0;
    tokenData.forEach((tx) => {
      total += tx.balance * tx.rate * rate;
    })

    setTotalPrice(total);
    setAnimating(false);
    setAnimatingH(false);

    (async () => {
      //ストレージに格納しておく
      try {
        await AsyncStorage.setItem(Const.KEY_TOKENDATA, JSON.stringify(tokenData));
      } catch (error) {
        console.log("AsyncStorage setCurrentWalletState error..")
        console.log(error);
      }
    })();

  }, [tokenData, rate])

  /**
   * rateDataからrateを得る
   */
  React.useEffect(() => {
    console.log("currencyContext.currentCurrencyState run " + currencyContext.currentCurrencyState)
    rate_set(rateData);

  }, [currencyContext.currentCurrencyState])

  /**
   * rateをsetする
   */
  function rate_set(data) {
    //console.log(data)
    if (data !== null) {
      let r = 1;
      data.forEach((tx) => {
        if (tx.currencyPairCode === "USDJPY") {
          if (currencyContext.currentCurrencyState == "JPY") {
            r = tx.high
          }
        } else if (tx.currencyPairCode === "EURUSD") {
          if (currencyContext.currentCurrencyState == "EUR") {
            r = (1 / tx.high)
          }
        }
      })
      console.log("rate=" + r)
      setRate(r);
    }
  }

  /*
  * 現在の持ち通貨を得る
  */
  async function getBarance(address) {
    console.log("getBarance run..")
    // 接続するノード（INFURA および Etherscan のノードに同時に接続）
    let provider = ethers.getDefaultProvider(Const.network)

    var arrTokenData = tokenData.slice();
    var balance = "";
    var symbol = "";
    var datagetcount = 0;

    Const.token_data.forEach(async (tx, index) => {
      if (tx.contract !== "") {
        let contract = new ethers.Contract(tx.contract, Const.erc20abi, provider);

        contract.balanceOf(address)
          .then((balance) => {
            let balanceInEth = ethers.utils.formatEther(balance)
            console.log("Balance:", balanceInEth)

            arrTokenData[tx.key]["balance"] = balanceInEth;

            datagetcount++;
            if (datagetcount == Const.token_data.length) {
              setTokenData(arrTokenData)
              console.log("getBalance end..")
            }
          })
      } else {
        //contractが無い場合はEtherium
        // 渡したアドレスの Ether 残高を取得
        provider.getBalance(address)
          .then((balance) => {
            let balanceInEth = ethers.utils.formatEther(balance)
            console.log("Balance:", balanceInEth)

            arrTokenData[tx.key]["balance"] = balanceInEth;

            datagetcount++;
            if (datagetcount == Const.token_data.length) {
              setTokenData(arrTokenData)
              console.log("getBalance end..")
            }
          })
      }
    })
  }

  /**
   * トランザクション履歴を取得する
   * @param {string} address 
   * @param {int} select_token 
   */
  async function getHistory(address, select_token) {
    console.log("getHistory run.. address=" + address + " select_token=" + select_token)

    var history_data = [];

    if (Const.token_data[select_token]["symbol"] == "ETH") {
      //eth test用address
      //address = "0x845d9dfe4A49fAc37DddcC2A4a54e94B909dB38f"

      // Etherscan provider を取得
      let etherscanProvider = new ethers.providers.EtherscanProvider(Const.network, Const.etherscan_api_key)

      // トランザクション履歴を取得
      axios.get(etherscanProvider.baseUrl + "/api?module=account&action=txlist&address=" + address + "&page=1&offset=1000&sort=desc&startblock=0&endblock=99999999&apikey=" + etherscanProvider.apiKey)
        .then((history) => {
          if (history.data.message !== "OK") {
            console.log(history);
            history.data.result = [];
          }
          //console.log(history.data.result)
          history.data.result.forEach((tx, index) => {
            //console.log(tx)
            console.log("toAddresses=" + tx.from);
            console.log("fromAddresses=" + tx.to);
            let amounts = ethers.utils.formatEther(tx.value)
            console.log("amounts=" + amounts);
            console.log("timeStamp=" + tx.timeStamp)
            console.log("txreceipt_status=" + tx.txreceipt_status)
            moment.locale(locale);
            let format_date = moment(new Date(tx.timeStamp * 1000)).format("LLL");
            console.log("format_date=" + format_date)
            console.log("hash=" + tx.hash)
            if (amounts > 0)
              history_data.push({ key: index, fromAddress: tx.from, toAddress: tx.to, amount: amounts.toString(), status: tx.txreceipt_status, timestamp: tx.timeStamp, hash: tx.hash });
          })

          setHistoryData(history_data);
          setRefreshing(false);
        });
    } else {
      console.log("contract=" + Const.token_data[select_token].contract)

      let etherscanProvider = new ethers.providers.EtherscanProvider(Const.network, Const.etherscan_api_key)
      let contractAddress = Const.token_data[select_token].contract;

      // creating the interface of the ABI
      const iface = new ethers.utils.Interface(Const.erc20abi);
      // get all events from interface
      const events = iface.events;
      // filter for Transfer
      const transfer = events["Transfer"];
      // get event topic
      const eventTopic = transfer.topic;
      const fromTopic = ethers.utils.hexZeroPad(address, 32);
      const toTopic = ethers.utils.hexZeroPad(address, 32);

      let url = etherscanProvider.baseUrl + "/api?module=logs&action=getLogs&address=" + contractAddress + "&topic0=" + [eventTopic] + "&topic1=" + [fromTopic] + "&topic1_2_opr=or&topic2=" + [toTopic] + "&fromBlock=0&toBlock=latest&apikey=" + etherscanProvider.apiKey;

      axios.get(url)
        .then((res) => {
          if (res.data.message !== "OK") {
            console.log(res.data);
            res.data.result = [];
          }
          let history = res.data.result;
          const decodedEvents = history.map(log => iface.parseLog(log));

          //console.log(decodedEvents)
          decodedEvents.forEach((event, index) => {
            //console.log(event);
            if (event == null)
              return;

            let toAddresses = event["values"]["to"];
            let fromAddresses = event["values"]["from"];
            let amounts = ethers.utils.formatEther(event["values"]["amount"]);

            //"0xa786629c7d08FD0a6d7FA731ce94E9e3FbE26823"
            if (toAddresses === address || fromAddresses === address) {
              console.log("toAddresses=" + toAddresses);
              console.log("fromAddresses=" + fromAddresses);
              console.log("amounts=" + amounts);
              let ts = parseInt(history[index].timeStamp, 16);
              console.log("timeStamp=" + ts);

              let transactionHash = history[index]["transactionHash"];

              history_data.push({ key: index, fromAddress: fromAddresses, toAddress: toAddresses, amount: amounts.toString(), status: 0, timestamp: ts, hash: transactionHash });

            }
          });

          history_data.sort(function (a, b) {
            if (a.timestamp > b.timestamp) return -1;
            if (a.timestamp < b.timestamp) return 1;
            return 0;
          });

          setHistoryData(history_data);
          setRefreshing(false);

        });
    }
  }

  /**
   * ウォレットのアイテムを返す
   * @param {*} param0 
   */
  const _renderItem = ({ item, index }) => {
    console.log("_renderItem=" + index + " select_token=" + item.select_token)
    return (
      <View style={styles.pageShadow}>
        <TouchableOpacity activeOpacity={1} onPress={() => navigation.navigate(path.CURRENCYLIST, { currentWallet: walletContext.currentWalletState })}>
          <View style={styles.page}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <Image style={styles.icon} source={tokenData[item.select_token]["icon"]} />
              <TouchableOpacity style={{ width: win.width - 104, height: 54, justifyContent: "center", }} onPress={() => setRenameShow(true)}>
                <Text style={{ fontSize: 16, fontWeight: "bold" }} >{item.name} <AntDesign name="edit" size={17} color="blue" /></Text>

              </TouchableOpacity>
            </View>
            <View style={{ paddingLeft: 10, }}>
              {walletContext.currentWalletState == index && animating == false ?
                <>
                  <Text style={{ fontSize: 27, }}>
                    {tokenData[item.select_token]["balance"]} {tokenData[item.select_token]["symbol"]}
                  </Text>
                  <Text style={{ fontSize: 16, paddingLeft: 2 }} >
                    {orgFloor((tokenData[item.select_token]["balance"] * tokenData[item.select_token]["rate"]) * rate, 4)} {currencyContext.currentCurrencyState}
                  </Text>
                </>
                :
                <>
                  <Text style={{ fontSize: 28, }}>...</Text>
                </>}
            </View>

            <Text style={{ fontSize: 30, }}></Text>
            <View style={{ flexDirection: "row", justifyContent: "center" }}>
              <TouchableOpacity style={[styles.button, { marginRight: 10 }]} onPress={() => navigation.navigate(path.RECEIVE_COIN, { wallet: wallet[walletContext.currentWalletState] })}>
                <Text style={{ color: '#fff', textAlign: 'center' }}>{t("STR_RECV")}</Text>
              </TouchableOpacity>

              <TouchableOpacity style={[styles.button, { backgroundColor: '#16a2b3', }]} onPress={() => navigation.navigate(path.CAMERA)}>
                <Text style={{ color: '#fff', textAlign: 'center' }}>{t("STR_SEND")}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  }

  /**
   * n文字以上を省略
   * omit(省略したい文字列)(何文字以上で省略するか)(省略した時の記号)
   * @param {*} text 
   */
  const omit = text => len => ellipsis =>
    ([...ellipsis].length < len
      && [...text].length >= len)
      ? text.slice(0, len - [...ellipsis].length) + ellipsis
      : text

  /**
   * 取引履歴を表示する
   * @param {*} param0 
   */
  const _renderHistory = ({ item, index }) => {
    console.log("_renderHistory=" + index)

    let my_addr = wallet[walletContext.currentWalletState].address;
    let select_token = wallet[walletContext.currentWalletState].select_token;
    let symbol = tokenData[select_token]["symbol"]
    let tokenrate = tokenData[select_token]["rate"]
    let ethrate = tokenData[0]["rate"]

    return (
      <View style={{ marginHorizontal: 10, marginBottom: 20, }}>
        {item.fromAddress.toUpperCase() == my_addr.toUpperCase() ?
          <View style={{ marginLeft: win.width * 0.25 }}>
            <Text style={[styles.text, { color: "#707070" }]}>To:{omit(item.toAddress)(20)("…")}</Text>
            <TouchableOpacity style={styles.cell_send} onPress={() => navigation.navigate(path.TRANSACTIONLOG, { hash: item.hash, symbol: symbol, amount: item.amount, address: my_addr, usdjpyrate: rate, tokenrate: tokenrate, ethrate: ethrate })}>
              <HistryInner item={item} />
            </TouchableOpacity>
          </View>
          :
          <>
            <Text style={{ color: "#707070" }}>From:{omit(item.fromAddress)(20)("…")}</Text>
            <TouchableOpacity style={styles.cell_recv} onPress={() => navigation.navigate(path.TRANSACTIONLOG, { hash: item.hash, symbol: symbol, amount: item.amount, address: my_addr, usdjpyrate: rate, tokenrate: tokenrate, ethrate: ethrate })}>
              <HistryInner item={item} />
            </TouchableOpacity>
          </>
        }
      </View>
    );
  }

  const HistryInner = ({ item }) => {
    let select_token = wallet[walletContext.currentWalletState].select_token;
    moment.locale(locale);
    let format_date = moment(new Date(item.timestamp * 1000)).format("LLL");

    return (
      <>
        <Text style={{ color: "#707070" }}>{item.amount} {tokenData[select_token]["symbol"]}</Text>
        <Text style={{ color: "#707070" }}>{format_date}</Text>
      </>
    )
  }
  /**
   * ページが変更された時のイベントハンドラ
   * @param {*} position 
   */
  const onPageSelected = async (position) => {
    console.log("onPageSelected=" + position)
    //コンテキスト更新
    walletContext.setCurrentWalletState(position);
    //アニメーション開始
    setAnimating(true);
    //トークンの情報を取得
    getBarance(wallet[position]["address"]);

    //初期化
    setHistoryData([]);
    //アニメーション開始
    setAnimatingH(true);
    //履歴を取得
    getHistory(wallet[position]["address"], wallet[position].select_token);

    //ストレージに格納しておく
    try {
      await AsyncStorage.setItem(Const.KEY_CURRENT_WALLET, position.toString());
      await AsyncStorage.setItem(Const.KEY_TOKENDATA, JSON.stringify(tokenData));
    } catch (error) {
      console.log("AsyncStorage setCurrentWalletState error..")
      console.log(error);
    }
  };

  const onOkPress = async () => {
    console.log("walletname run.. " + walletname)
    console.log("walletContext.currentWalletState=" + walletContext.currentWalletState)
    if (walletname.length == 0 || walletname > 30) {
      setRenameError(true);
      return;
    }
    wallet[walletContext.currentWalletState].name = walletname;

    try {
      console.log(JSON.stringify(wallet))
      await AsyncStorage.setItem(Const.KEY_WALLET, JSON.stringify(wallet));
    } catch (error) {
      console.log("AsyncStorage error..")
      console.log(error);
    }

    setRenameShow(false);
  };

  /**
 * 任意の桁で切り捨てする関数
 * @param {number} value 切り捨てする数値
 * @param {number} base どの桁で切り捨てするか（10→10の位、0.1→小数第１位）
 * @return {number} 切り捨てした値
 */
  function orgFloor(value, n) {
    return Math.floor(value * Math.pow(10, n)) / Math.pow(10, n);
  }

  /**
   * 上に引っ張って更新する際のイベントハンドラ
   */
  const onRefresh = () => {
    setRefreshing(true);

    let position = walletContext.currentWalletState;
    console.log("position = " + position)
    //アニメーション開始
    setAnimating(true);
    //トークンの情報を取得
    getBarance(wallet[position]["address"]);

    //初期化
    setHistoryData([]);
    //アニメーション開始
    setAnimatingH(true);
    //履歴を取得
    getHistory(wallet[position]["address"], wallet[position].select_token);

  }

  return (
    <ScrollView
      style={styles.container}
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }
    >
      <View style={{ flexDirection: "row", marginVertical: 15, }}>
        <Text style={{ width: 230, paddingLeft: 10, textAlignVertical: "center" }}>
          {t("STR_TOTAL_AMOUNT")}：{orgFloor(totalprice, 4)}  {currencyContext.currentCurrencyState}
        </Text>
        <View style={{ alignItems: "flex-end", width: win.width - 230, paddingRight: 10 }}>
          <TouchableOpacity
            style={[{ width: 106, height: 36, alignItems: "center", backgroundColor: "#EFEFEF", borderRadius: 6, justifyContent: "center", }]}
            onPress={() => navigation.navigate(path.CURRENCYLIST, { currentWallet: walletContext.currentWalletState })}>
            <Text style={{ color: "#000" }}>{t("CURRENCYLIST")}</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={{}}>
        {(wallet !== null ?
          <>
            {animating == true ?
              <ActivityIndicator
                animating={animating}
                color='#0000EE'
                size="large"
                style={styles.activityIndicator} />
              : null}
            <Carousel
              firstItem={walletContext.currentWalletState}
              itemWidth={win.width * 0.9}
              sliderWidth={win.width}
              loop={false}
              data={wallet}
              renderItem={_renderItem}
              onSnapToItem={onPageSelected}>
            </Carousel>
          </>
          : null)}
      </View>

      <View style={[_style.commonstyle.borderline, { paddingTop: 40 }]}></View>

      <View style={{ paddingVertical: 10, flexDirection: "row", backgroundColor: "#fff", }}>
        <Text style={{ width: win.width / 2, paddingLeft: 10, color: "#707070" }}>{t("TOKENHISTORY")}</Text>
        <TouchableOpacity style={{ width: win.width / 2, alignItems: "flex-end", paddingRight: 10 }} onPress={() => navigation.navigate(path.TOKENHISTORY, { rate: rate, tokenData: tokenData })}>
          <Text style={_style.commonstyle.textlink}>{t("STR_SHOW_ALL")}</Text>
        </TouchableOpacity>
      </View>

      {animatingH == true ?
        <ActivityIndicator
          animating={animatingH}
          color='#0000EE'
          size="large"
          style={styles.activityIndicatorH} />
        :
        <FlatList
          style={styles.listcontainer}
          data={historyData}
          removeClippedSubviews={false}
          keyExtractor={data => `${data.key}`}
          renderItem={_renderHistory}
          scrollEnabled={false}
        />
      }
      <Modal animationType="slide"
        transparent={true}
        visible={renameShow}>
        <View style={{ position: "absolute", backgroundColor: "#000", opacity: 0.3, height: win.height, width: win.width }} />
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
          <View style={styles.modalView}>
            <Text style={{ fontSize: 16, fontWeight: "bold" }}>{t("WALLET_RENAME")}</Text>

            {wallet != null ?
              <TextInput
                style={{ width: 250, borderColor: 'gray', borderWidth: 1, marginTop: 20, textAlignVertical: 'top', fontSize: 18 }}
                defaultValue={wallet[walletContext.currentWalletState].name}
                onChangeText={text => onChangeText(text)}
              />
              : null}
            <View style={{ flexDirection: "row", marginTop: 40 }}>
              <TouchableOpacity style={[styles.button, { marginRight: 10 }]} onPress={() => setRenameShow(false)}>
                <Text style={{ color: "#fff" }}>{t("STR_CANCEL")}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.button} onPress={() => onOkPress()}>
                <Text style={{ color: "#fff" }}>OK</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
      <Dialog
        contentStyle={
          {
            alignItems: "center",
            justifyContent: "center",
          }
        }
        visible={(renameError === false) ? false : true}
        title={t("WALLET_RENAME_ERR_TITLE")}
        onTouchOutside={() => setRenameError(false)}
      >
        <View>
          <Text>{t("WALLET_RENAME_ERR_STR")}</Text>
        </View>
      </Dialog>
    </ScrollView>
  );
}

/**
 * ローカルのスタイルシートを定義する
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  activityIndicator: {
    position: 'absolute',
    zIndex: 1,
    left: win.width * 0.05,
    top: 0,
    bottom: 0,
    width: win.width * 0.9,
    alignItems: 'center',
    justifyContent: 'center',
  },
  activityIndicatorH: {
    left: win.width * 0.05,
    top: 0,
    bottom: 0,
    width: win.width * 0.9,
    alignItems: 'center',
    justifyContent: 'center',
  },
  pageShadow: {
    paddingVertical: 1.2,
    borderRadius: 16,
    backgroundColor: 'transparent',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 4,
    elevation: 3,

  },
  page: {
    backgroundColor: '#fff',
    borderRadius: 16,

  },
  listcontainer: {
    flex: 2,
    backgroundColor: "#fff",
  },
  icon: {
    marginLeft: 10,
    width: 53,
    height: 53,
  },
  button: {
    marginBottom: 20,
    width: 147,
    backgroundColor: '#FF8F07',
    height: 38,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
    alignItems: "center",
    justifyContent: "center",
  },
  cell_send: {
    borderRadius: 12,
    width: win.width * 0.7,
    backgroundColor: "#EBEBEB",
    padding: 5
  },
  cell_recv: {
    borderRadius: 12,
    width: win.width * 0.7,
    backgroundColor: "#C7EEF4",
    padding: 5
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 20,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
});