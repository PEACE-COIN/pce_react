import * as React from 'react';
import { WebView } from 'react-native-webview';
import * as Localize from "../../../contexts/localize"

export default function PirvacyScreen() {
  const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);
  return (
    <WebView 
      source={{ uri: (locale === "ja-JP") ? 'https://wallet.a-d.systems/privacy_policy_ja.html' : 'https://wallet.a-d.systems/privacy_policy_en.html' }} 
      style={{ marginTop: 0 }} 
    />
  );
}