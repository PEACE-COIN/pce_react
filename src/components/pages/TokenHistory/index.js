import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, AsyncStorage, Dimensions, FlatList, ActivityIndicator, LogBox, Modal, RefreshControl } from 'react-native';
import * as Localize from "../../../contexts/localize"
import * as path from '../../../const/path';
import * as WalletContext from "../../../contexts/wallet";
import { ethers } from 'ethers';
import * as Const from "../../../const/const"
import axios from 'axios';
import moment from 'moment-with-locales-es6';

const win = Dimensions.get('window');

export default function TokenHistoryScreen({ navigation, route }) {
  const { rate, tokenData } = route.params;

  const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);

  const walletContext = React.useContext(WalletContext.Context);
  const [wallet, setWallet] = React.useState(null);
  const [animating, setAnimating] = React.useState(false);
  const [historyData, setHistoryData] = React.useState([]);
  const [refreshing, setRefreshing] = React.useState(false);

  /**
  * 現在のwallet設定を得る
  */
  React.useEffect(() => {
    //console.log("useEffect１ run..")
    /**
     * 現在のwallet設定を得る
     */
    async function getWalletSettings() {
      console.log("getWalletSettings run..")

      try {
        //ウォレットデータを取得
        let wallet_data = await AsyncStorage.getItem(Const.KEY_WALLET);
        wallet_data = JSON.parse(wallet_data);
        setWallet(wallet_data);
        let addr = wallet_data[walletContext.currentWalletState]["address"];
        //アニメーション開始
        setAnimating(true);
        //履歴を得る
        getHistory(addr);

      } catch (error) {
        console.log(error);
      }
    }

    getWalletSettings();
  }, [])

  var history_data = [];
  var history_data_count = 0;
  /**
   * トランザクション履歴を取得する
   * @param {string} address 
   * @param {int} select_token 
   */
  async function getHistory(address) {
    console.log("getHistory run.. address=" + address)

    history_data_count = 0;
    var data_key = 0;

    Const.token_data.forEach(async (tokens, index) => {

      if (tokens["symbol"] == "ETH") {
        //eth test用address
        //address = "0x845d9dfe4A49fAc37DddcC2A4a54e94B909dB38f"

        // Etherscan provider を取得
        let etherscanProvider = new ethers.providers.EtherscanProvider(Const.network, Const.etherscan_api_key)

        // トランザクション履歴を取得
        axios.get(etherscanProvider.baseUrl + "/api?module=account&action=txlist&address=" + address + "&page=1&offset=1000&sort=desc&startblock=0&endblock=99999999&apikey=" + etherscanProvider.apiKey)
          .then((history) => {
            if (history.data.message !== "OK") {
              console.log(history);
              history.data.result = [];
            }
            //console.log(history.data.result)
            history.data.result.forEach((tx, index) => {
              //console.log(tx)
              console.log("toAddresses=" + tx.from);
              console.log("fromAddresses=" + tx.to);
              let amounts = ethers.utils.formatEther(tx.value)
              console.log("amounts=" + amounts);
              console.log("timeStamp=" + tx.timeStamp)
              console.log("txreceipt_status=" + tx.txreceipt_status)
              moment.locale(locale);
              let format_date = moment(new Date(tx.timeStamp * 1000)).format("LLL");
              console.log("format_date=" + format_date)
              console.log("hash=" + tx.hash)

              if (amounts > 0) {
                history_data.push({ key: data_key, fromAddress: tx.from, toAddress: tx.to, amount: amounts.toString(), status: tx.txreceipt_status, timestamp: tx.timeStamp, hash: tx.hash, symbol: tokens["symbol"] });
                data_key++;
              }
            })

            console.log(tokens["symbol"] + "historyDataCnt =" + history_data_count)
            history_data_count++;

            if (Const.token_data.length == history_data_count) {
              //console.log(history_data)
              history_data.sort(function (a, b) {
                if (a.timestamp > b.timestamp) return -1;
                if (a.timestamp < b.timestamp) return 1;
                return 0;
              });

              setHistoryData(history_data);
              setAnimating(false);
            }
          });
      } else {

        let etherscanProvider = new ethers.providers.EtherscanProvider(Const.network, Const.etherscan_api_key)
        let contractAddress = tokens.contract;

        // creating the interface of the ABI
        const iface = new ethers.utils.Interface(Const.erc20abi);
        // get all events from interface
        const events = iface.events;
        // filter for Transfer
        const transfer = events["Transfer"];
        // get event topic
        const eventTopic = transfer.topic;
        const fromTopic = ethers.utils.hexZeroPad(address, 32);
        const toTopic = ethers.utils.hexZeroPad(address, 32);
        let url = etherscanProvider.baseUrl + "/api?module=logs&action=getLogs&address=" + contractAddress + "&topic0=" + [eventTopic] + "&topic1=" + [fromTopic] + "&topic1_2_opr=or&topic2=" + [toTopic] + "&fromBlock=0&toBlock=latest&apikey=" + etherscanProvider.apiKey;

        axios.get(url)
          .then((res) => {
            //console.log(res)
            if (res.data.message !== "OK") {
              console.log(res.data);
              res.data.result = [];
            }
            let history = res.data.result;
            const decodedEvents = history.map(log => iface.parseLog(log));

            //console.log(history)
            decodedEvents.forEach((event, index) => {
              //console.log(parseInt(history[index].blockNumber));
              if (event == null)
                return;

              let toAddresses = event["values"]["to"];
              let fromAddresses = event["values"]["from"];
              let amounts = ethers.utils.formatEther(event["values"]["amount"]);

              //"0xa786629c7d08FD0a6d7FA731ce94E9e3FbE26823"
              if (toAddresses === address || fromAddresses === address) {

                console.log("toAddresses=" + toAddresses);
                console.log("fromAddresses=" + fromAddresses);
                console.log("amounts=" + amounts);
                let ts = parseInt(history[index].timeStamp, 16);
                console.log("timeStamp=" + ts);

                let format_date = moment(new Date(ts * 1000)).format("LLL");
                console.log("format_date=" + format_date)

                let transactionHash = history[index]["transactionHash"];

                history_data.push({ key: data_key, fromAddress: fromAddresses, toAddress: toAddresses, amount: amounts.toString(), status: 0, timestamp: ts, hash: transactionHash, symbol: tokens["symbol"] });
                data_key++;
              }
            });
            console.log(tokens["symbol"] + "historyDataCnt =" + history_data_count)
            history_data_count++;
            if (Const.token_data.length == history_data_count) {
              //console.log(history_data)
              history_data.sort(function (a, b) {
                if (a.timestamp > b.timestamp) return -1;
                if (a.timestamp < b.timestamp) return 1;
                return 0;
              });

              setHistoryData(history_data);
              setAnimating(false);
            }

          });
      }

    });

  }

  /**
 * n文字以上を省略
 * omit(省略したい文字列)(何文字以上で省略するか)(省略した時の記号)
 * @param {*} text 
 */
  const omit = text => len => ellipsis =>
    ([...ellipsis].length < len
      && [...text].length >= len)
      ? text.slice(0, len - [...ellipsis].length) + ellipsis
      : text

  /**
   * 取引履歴を表示する
   * @param {*} param0 
   */
  const _renderHistory = ({ item, index }) => {
    console.log("_renderHistory=" + index)
    let my_addr = wallet[walletContext.currentWalletState].address;
    let ethrate = tokenData[0]["rate"]
    let tokenrate;

    tokenData.forEach((tx) => {
      if (tx.symbol == item.symbol) {
        tokenrate = tx.rate
        return;
      }
    })

    return (
      <View style={{ marginHorizontal: 10, marginBottom: 20, }}>
        {item.fromAddress.toUpperCase() == my_addr.toUpperCase() ?
          <View style={{ marginLeft: win.width * 0.25 }}>
            <Text style={[styles.text, { color: "#707070" }]}>To:{omit(item.toAddress)(20)("…")}</Text>
            <TouchableOpacity style={styles.cell_send} onPress={() => navigation.navigate(path.TRANSACTIONLOG, { hash: item.hash, symbol: item.symbol, amount: item.amount, address: my_addr, usdjpyrate: rate, tokenrate: tokenrate, ethrate: ethrate })}>
              <HistryInner item={item} />
            </TouchableOpacity>
          </View>
          :
          <>
            <Text style={{ color: "#707070" }}>From:{omit(item.fromAddress)(20)("…")}</Text>
            <TouchableOpacity style={styles.cell_recv} onPress={() => navigation.navigate(path.TRANSACTIONLOG, { hash: item.hash, symbol: item.symbol, amount: item.amount, address: my_addr, usdjpyrate: rate, tokenrate: tokenrate, ethrate: ethrate })}>
              <HistryInner item={item} />
            </TouchableOpacity>
          </>
        }
      </View>
    );
  }

  const HistryInner = ({ item }) => {
    moment.locale(locale);
    let format_date = moment(new Date(item.timestamp * 1000)).format("LLL");

    return (
      <>
        <Text style={{ color: "#707070" }}>{item.amount} {item["symbol"]}</Text>
        <Text style={{ color: "#707070" }}>{format_date}</Text>
      </>
    )
  }

  /**
   * 上に引っ張って更新する際のイベントハンドラ
   */
  const onRefresh = async () => {
    setRefreshing(true);
    //履歴を得る
    let addr = wallet[walletContext.currentWalletState]["address"];
    await getHistory(addr);
    setRefreshing(false);
  }

  return (
    <View style={styles.container}>
      {animating == true ?
        <ActivityIndicator
          animating={animating}
          color='#0000EE'
          size="large"
          style={styles.activityIndicatorH} />
        :
        <FlatList
          style={styles.listcontainer}
          data={historyData}
          removeClippedSubviews={false}
          keyExtractor={data => `${data.key}`}
          renderItem={_renderHistory}
          scrollEnabled={true}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
        />
      }
    </View>
  );
}


/**
 * ローカルのスタイルシートを定義する
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  activityIndicator: {
    position: 'absolute',
    zIndex: 1,
    left: win.width * 0.05,
    top: 0,
    bottom: 0,
    width: win.width * 0.9,
    alignItems: 'center',
    justifyContent: 'center',
  },
  activityIndicatorH: {
    left: win.width * 0.05,
    top: 0,
    bottom: 0,
    width: win.width * 0.9,
    alignItems: 'center',
    justifyContent: 'center',
  },
  pageShadow: {
    borderRadius: 16,
    backgroundColor: 'transparent',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  page: {
    backgroundColor: '#fff',
    borderRadius: 16,
  },
  listcontainer: {
    flex: 2,
    backgroundColor: "#fff",
  },
  button: {
    marginBottom: 20,
    width: 147,
    backgroundColor: '#FF8F07',
    height: 38,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
    alignItems: "center",
    justifyContent: "center",
  },
  cell_send: {
    borderRadius: 12,
    width: win.width * 0.7,
    backgroundColor: "#EBEBEB",
    padding: 5
  },
  cell_recv: {
    borderRadius: 12,
    width: win.width * 0.7,
    backgroundColor: "#C7EEF4",
    padding: 5
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 20,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
});