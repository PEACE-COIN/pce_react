import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View ,Platform} from 'react-native';
import '../shim.js'
import * as firebase from 'firebase';
import Constants from 'expo-constants';

//String.normalizeが動かないので・・
String.prototype.normalize = function(form) { return require('unorm')[String(form).toLowerCase()](this); }

//import screens
import Index from './components/test';

export default function App() {

  console.log(Constants.appOwnership)
  console.log(Platform.OS)

  const env = Constants.manifest.releaseChannel
  console.log("env=" + env) 
  
  // Initialize Firebase
  const firebaseConfig = {
    apiKey: "AIzaSyBPi1L_sKaQYtFj0kNgfjNCvGAoLTpzdas",
    authDomain: "peace-coin-wallet-new.firebaseapp.com",
    databaseURL: "https://peace-coin-wallet-new.firebaseio.com",
    projectId: "peace-coin-wallet-new",
    storageBucket: "peace-coin-wallet-new.appspot.com",
    messagingSenderId: "sender-id",
    appId: "1:336539627559:"+Platform.OS+":94a29451333dcaeed6de02",
    measurementId: "G-measurement-id"
  };

  if (firebase.apps.length === 0) {
    firebase.initializeApp(firebaseConfig);
  }

  firebase.auth().signInAnonymously().catch(function(error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;

    console.log(errorCode + ":" + errorMessage)
    // ...
  });

  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      // User is signed in.
      var isAnonymous = user.isAnonymous;
      var uid = user.uid;
      console.log("uid=" + uid)
      // ...
    } else {
      // User is signed out.
      // ...
      console.log("User is signed out")
    }
    // ...
  });

      return (
        <Index/>
      );

}