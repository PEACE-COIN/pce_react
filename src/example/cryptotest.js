/**
 * 共通ヘッダーのUI部分を表示するコンポーネント
 */
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import axios from 'axios';
import { StyleSheet, Text, View , Image, Button} from 'react-native';
import { ethers } from 'ethers';
import { ja,en,es } from 'ethers/wordlists';
import { JSHash, JSHmac, CONSTANTS } from "react-native-hash";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

const md5Hex = require('md5-hex');

const cmc_api_key = "cedb772c-4503-422b-87d5-3d8495c7d48e";
const lbank_api_key = "b2371d78-6218-42f4-aa80-99f1c06c6cf8"

function SettingsScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Settings Screen</Text>
      <Button
        title="Go to Profile"
        onPress={() => navigation.navigate('Profile')}
      />
    </View>
  );
}

function ProfileScreen({ navigation }) {
  React.useEffect(
    () => navigation.addListener('focus', () => alert('Screen was focused')),
    []
  );

  React.useEffect(
    () => navigation.addListener('blur', () => alert('Screen was unfocused')),
    []
  );

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Profile Screen</Text>
      <Button
        title="Go to Settings"
        onPress={() => navigation.navigate('Settings')}
      />
    </View>
  );
}

function HomeScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                  <Image 
                    source={require('../../assets/images/smile_logo.png')} 
                    style={{width: 66,height: 58}}
                    />
                  <Text>Open up index.js to start working on your app!</Text>
                  <StatusBar style="auto" />
                  <Button
                    title="Go to Details"
                    onPress={() => {navigation.navigate('Details',{
                      itemId: 86,
                      otherParam: 'anything you want here',
                    })
                  }}
                  />
              </View>

  );
}

function DetailsScreen({ route, navigation }) {
  const { itemId , otherParam} = route.params;

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Details Screen</Text>
      <Text>itemId: {JSON.stringify(itemId)}</Text>
      <Text>otherParam: {JSON.stringify(otherParam)}</Text>
      <Button
        title="Go to Details... again"
        onPress={() => navigation.push('Details')}
      />
      <Button title="Go to Home" onPress={() => navigation.navigate('Home')} />
      <Button title="Go back" onPress={() => navigation.goBack()} />

      <Button
        onPress={() => navigation.navigate('MyModal')}
        title="Open Modal"
      />
    </View>
  );
}

const Tab = createBottomTabNavigator();
const SettingsStack = createStackNavigator();
const HomeStack = createStackNavigator();

class Index extends React.Component {
    constructor(props) {
        super(props);
    }

    async componentDidMount(){
        console.log("componentDidMount run..");

        //新規アドレス作成
        // Chose the length of your mnemonic:
        //   - 16 bytes => 12 words (* this example)
        //   - 20 bytes => 15 words
        //   - 24 bytes => 18 words
        //   - 28 bytes => 21 words
        //   - 32 bytes => 24 words
        
        let bytes = ethers.utils.randomBytes(32);
        let randomMnemonic = ethers.utils.HDNode.entropyToMnemonic(bytes, ja)
        console.log(randomMnemonic);
        this.makeWallet(randomMnemonic, ja);
        

        //既存アドレス復元（mnemonic）
        //address:0xEF6ff5193FCA6bCef004590EE47559204e6D090e
        /*
        let randomMnemonic = "output wrist vault fix gift snack brass outer account tail wagon rifle"
        this.makeWallet(randomMnemonic, en);
        */

        //this.getPrices()
        //this.getEthPricesByApi("JPY")
        //this.getPCEPricesByApi()

        // 残高を取得するアドレス（何もしなくてもENSの名前解決をしてくれる！）
        let address = "0x845d9dfe4A49fAc37DddcC2A4a54e94B909dB38f"  

        //this.getBarance(address)
        //this.getHistory(address)
      }
      
      /*
      * ニーモニックキーからウォレットを生成する。
      * @randomMnemonic ニーモニックキー 
      * @word ワードリスト
      */
      makeWallet(randomMnemonic, word) {
        console.log("makewallet run..");
        if(word == null)
            word = en
        let isValid = ethers.utils.HDNode.isValidMnemonic(randomMnemonic, word)
        console.log(isValid);
        if(isValid){
            try{
                let wallet = ethers.utils.HDNode.fromMnemonic(randomMnemonic,word,null);
                // ウォレットのアドレスを取得
                let address = wallet.address
                console.log("address:", address)
                // ウォレットのニーモニックを取得
                let mnemonic = wallet.mnemonic
                console.log("mnemonic:", mnemonic)
                // ウォレットの秘密鍵を取得
                let privateKey = wallet.privateKey
                console.log("privateKey:", privateKey)
            }catch(e){
                console.log(e)
            }
        }
      }
      /*
      * 現在の通貨のレートをUSDで得る
      */
      getPrices(){
        console.log("makewallet run..");

        // 接続するネットワーク
        // You can use any standard network name
        //  - "homestead"
        //  - "rinkeby"
        //  - "ropsten"
        //  - "kovan"
        //  - "goerli"
        let network = 'homestead'
        
        // Etherscan のノード
        let etherscanProvider = new ethers.providers.EtherscanProvider(network);
        
        // Ether の USD 価格を取得
        etherscanProvider.getEtherPrice()
        .then(function(price) {
            console.log("ETH price in USD:", price);
        });
      }

      /*
      * 現在の通貨のレートを得る
      * @currency "USD" "JPY" 等の通貨単位
      */
      getEthPricesByApi(currency){
        
        const url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest';
        let qString = "?CMC_PRO_API_KEY=" + cmc_api_key + "&symbol=ETH&convert=" + currency;

        console.log(url + qString)

        axios.get(url + qString, null )
        .then((results) => {
          //console.log(results)
          let data = results.data;
          console.log('ETH/' + currency, data["data"]["ETH"]["quote"][currency]["price"])
        },)
        .catch((error) => {
          console.log(error);
        });
      }

            /*
      * 現在の通貨のレートを得る
      * @currency "USD" "JPY" 等の通貨単位
      */
     getPCEPricesByApi(){
      const url = 'https://api.lbkex.com/v2/ticker.do';
          
      axios.get(url + "?symbol=eth_usdt")
      .then((results) => {
        console.log(results.data)
        let data = results.data;

        data.data.forEach((tx) => {
          console.log(tx);
        })
      },).catch((e) => {
        console.log(e)
      });
     }
     /*未使用 */
     getLbankUserByApi(){
        
      const timestampUrl = "https://api.lbkex.com/v2/timestamp.do"

      axios.get(timestampUrl, null )
      .then((results) => {
        //console.log(results)
        let data = results.data;
        let timestamp = data.data;
        let echostr = "P3LHfw6tUIYWc8R2VQNy0ilKmdg5pjhbxC7";
        
        const signature_method = "HmacSHA256"
        
        let parameters="api_key="+lbank_api_key+"&echostr="+echostr+"&signature_method="+signature_method+"&timestamp=" + timestamp

        let preparedStr = md5Hex(parameters)
        console.log(preparedStr.toUpperCase())
        let SecretKey = "MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAMeRtlFkHQ/qxFIeRPBcVmrhfJvJoAlfqSAtEgkHZpMJMEFhA7kWzrMlBJPGqSsjv0/FDCTiuXPWM/xHT+fz9orMg6NJUNiqsA/a4HqUXfsr9o6d2Ta0MqotokA4lXJvDJucM+JaBK1p4rnY8pB3CpwkuMQEjMl7KyzfB8vKgtYhAgMBAAECgYEAnkxmYYkXxFhkAP8hj8auhXj2blwwJT9L18noVGEaa+cN784UNexxaIgkaVNbPGzOg2e5ikiM8eUmtyWZrFNHasn21lNLg8kFiXslr/tWSKg2hgClPwFmiW6WmwIP5x3sDBQjN+gfjbHYrkixXF7jTEpekoxA0Fn2cyPsyfvwAAECQQDyfmgzSoCDWOa3oE8gTj9WCY6gZPPHnJvFdpoBQnn7DdFKD8t0pbGl2FGSLqo/PMZC4N/mTqp6vT2Rh0/tN5qBAkEA0q9EBMKMDmVUyTGd72vS2cprHScLqD6zI4qMVEQw4UQoemhsQJO/FoGK3CjPDS2sDeoHXULJYP8qWMULYhcroQJALsi8icjB6pVoTd0b5vuxtIO7hK51afmJBBvRspSEvC76RfOuKR8emwSVGELwjarIBXclO2XJ9kw0aLNWdEnaAQJBALytLwngPFNMFhET+S+H7U0TxLdX10210PHcSu3QH8ItKt7skz2F0xRRsMyXU02nADV5x1ySyguul/rlg9s1iuECQQCkuGY2diaYUfEx7CiMoKQXH2GgB6Pd2jZM8WTTZPBaWk9yZ1tcOwVacVyPM62r0JK7GsPuXQjCoQ9q3JffAw/Y"

        JSHmac(preparedStr, SecretKey, CONSTANTS.HmacAlgorithms.HmacSHA256)
        .then((hash) => {
          console.log("hash:"+hash)
          const url = 'https://api.lbkex.com/v2/ticker.do';
          
          axios.get(url + "?symbol=eth_btc")
          .then((results) => {
            console.log(results.data)
          },).catch((e) => {
            console.log(e)
          });
        })
        .catch(e => console.log(e));

      },)
      .catch((error) => {
        console.log(error);
      });
    }
      /*
      * 現在の持ち通貨を得る
      */
      getBarance(address){
        // 接続するネットワーク
        let network = 'homestead'

        // 接続するノード（INFURA および Etherscan のノードに同時に接続）
        let provider = ethers.getDefaultProvider(network)
        
        // 渡したアドレスの Ether 残高を取得
        provider.getBalance(address)
        .then((balance) => {
            let balanceInEth = ethers.utils.formatEther(balance)
            console.log("Balance:", balanceInEth)
        })
        
      }

      getHistory(address){
        // 接続するネットワーク
        let network = 'homestead'
        
        // Etherscan provider を取得
        let etherscanProvider = new ethers.providers.EtherscanProvider(network)
                
        // トランザクション履歴を取得
        etherscanProvider.getHistory(address).then((history) => {
            history.forEach((tx) => {
                console.log(tx);
            })
        });
      }
    render() {
        console.log("render run..");
        return(
            <NavigationContainer>
              <Tab.Navigator>
                <Tab.Screen name="First">
                  {() => (
                    <SettingsStack.Navigator>
                      <SettingsStack.Screen
                        name="Settings"
                        component={SettingsScreen}
                      />
                      <SettingsStack.Screen name="Profile" component={ProfileScreen} />
                    </SettingsStack.Navigator>
                  )}
                </Tab.Screen>
                <Tab.Screen name="Second">
                  {() => (
                    <HomeStack.Navigator>
                      <HomeStack.Screen name="Home" component={HomeScreen} />
                      <HomeStack.Screen name="Details" component={DetailsScreen} />
                      <HomeStack.Screen name="MyModal" component={ModalScreen} />
                    </HomeStack.Navigator>
                  )}
                </Tab.Screen>
              </Tab.Navigator>
            </NavigationContainer>
        )
    }
}

function ModalScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text style={{ fontSize: 30 }}>This is a modal!</Text>
      <Button onPress={() => navigation.goBack()} title="Dismiss" />
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
});

export default Index;