import React from "react";
import { Text, Image } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import * as Localize from "../../contexts/localize"
import * as _style from "../../const/style";
import * as path from "../../const/path";
import * as screen from "../../components/pages";
import { HeaderRight } from "../Header";

const Stack = createStackNavigator();

function HomeNavigator(){
    const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);

    return(
        <Stack.Navigator initialRouteName={path.HOME} screenOptions={_style.headerstyle}>
            <Stack.Screen 
                name={path.HOME} 
                component={screen.HomeScreen} 
                options={{ 
                    title: t(path.HOME), 
                    headerRight:() => <HeaderRight />,
                }} 
            />
            <Stack.Screen name={path.TOKENHISTORY} options={{ title: t(path.TOKENHISTORY)}}  component={screen.TokenHistoryScreen} />
            <Stack.Screen name={path.CURRENCYLIST} options={{ title: t(path.CURRENCYLIST)}}  component={screen.CurrencyListScreen} />
            <Stack.Screen name={path.RESTORE_WALLET} options={{ title: t(path.RESTORE_WALLET)}}  component={screen.RestoreWalletScreen} />
            <Stack.Screen name={path.MAKEWALLET} options={{ title: t(path.MAKEWALLET)}}  component={screen.MakeWalletScreen} />
            <Stack.Screen name={path.TRANSACTIONLOG} options={{ title: t(path.TRANSACTIONLOG)}}  component={screen.TransactionLogScreen} />
            
        </Stack.Navigator>
    );
}

export default HomeNavigator;