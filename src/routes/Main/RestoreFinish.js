import React from "react";
import { createStackNavigator } from '@react-navigation/stack';
import { PASSCODE_INPUT, PASSCODE_CONFIRM, TOUCHID_INPUT} from "../../const/path";
import * as Localize from "../../contexts/localize"
import * as _style from "../../const/style";
import * as screen from "../../components/pages";

const restoreStack = createStackNavigator();

function RestoreFinishNavigator() {
    const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);

    return (
        <restoreStack.Navigator screenOptions={_style.headerstyle}>
            <restoreStack.Screen name={PASSCODE_INPUT} options={{ title: t(PASSCODE_INPUT) }} component={screen.PasscodeInputScreen} />
            <restoreStack.Screen name={PASSCODE_CONFIRM} options={{ title: t(PASSCODE_CONFIRM) }} component={screen.PasscodeConfirmScreen} />
            <restoreStack.Screen name={TOUCHID_INPUT} options={{ title: t(TOUCHID_INPUT) }} component={screen.TouchidInputScreen} />
        </restoreStack.Navigator>
    );
}

export default RestoreFinishNavigator;