import React from "react";
import { createStackNavigator, StackCardInterpolationProps } from '@react-navigation/stack';
import * as Localize from "../../contexts/localize"
import * as _style from "../../const/style";
import * as path from "../../const/path";
import * as screen from "../../components/pages";

const Stack = createStackNavigator();

function AccountNavigator(){
    const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);

    return(
        <Stack.Navigator initialRouteName={path.ACCOUNT} screenOptions={_style.headerstyle}>
            <Stack.Screen name={path.ACCOUNT} options={{ title: t(path.ACCOUNT)}} component={screen.AccountScreen} />
            <Stack.Screen name={path.WALLET_RESET} options={{ title: t(path.WALLET_RESET)}} component={screen.WalletResetScreen} />
            <Stack.Screen name={path.BACKUP} options={{ title: t(path.BACKUP)}} component={screen.BackupScreen} />
            <Stack.Screen name={path.SECURITY} options={{ title: t(path.SECURITY)}} component={screen.SecurityScreen} />
            <Stack.Screen name={path.LANGUAGE} options={{ title: t(path.LANGUAGE)}} component={screen.LanguageScreen} />
            <Stack.Screen name={path.CURRENCY} options={{ title: t(path.CURRENCY)}} component={screen.CurrencyScreen} />
            <Stack.Screen name={path.PRIVACY} options={{ title: t(path.PRIVACY)}} component={screen.PrivacyScreen} />
            <Stack.Screen name={path.PORICY} options={{ title: t(path.PORICY)}} component={screen.PoricyScreen} />
            <Stack.Screen name={path.PASS_PHRASE_SHOW} options={{ title: t(path.PASS_PHRASE_SHOW) }} component={screen.PassPhraseShowScreen} />
            <Stack.Screen name={path.PASSCODE_INPUT} options={{ title: t(path.PASSCODE_INPUT)}} component={screen.PasscodeInputScreen} />
            <Stack.Screen name={path.PASSCODE_CONFIRM} options={{ title: t(path.PASSCODE_CONFIRM)}} component={screen.PasscodeConfirmScreen} />
        </Stack.Navigator>
    );
}

export default AccountNavigator;