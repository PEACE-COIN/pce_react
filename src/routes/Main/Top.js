import React from "react";
import { createStackNavigator, StackCardInterpolationProps } from '@react-navigation/stack';
import * as Localize from "../../contexts/localize"
import * as _style from "../../const/style";
import * as path from "../../const/path";
import * as screen from "../../components/pages";


const Stack = createStackNavigator();

function TopNavigator(){
    const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);

    return(
        <Stack.Navigator initialRouteName={path.TOP} headerMode="screen" screenOptions={_style.headerstyle}>
            <Stack.Screen name={path.TOP} component={screen.TopScreen} options={{headerShown: false}} />
            <Stack.Screen name={path.PASSCODE_INPUT} options={{ title: t(path.PASSCODE_INPUT)}} component={screen.PasscodeInputScreen} />
            <Stack.Screen name={path.PASSCODE_CONFIRM} options={{ title: t(path.PASSCODE_CONFIRM)}} component={screen.PasscodeConfirmScreen} />
            <Stack.Screen name={path.TOUCHID_INPUT} options={{ title: t(path.TOUCHID_INPUT)}} component={screen.TouchidInputScreen} />
            <Stack.Screen name={path.RESTORE_WALLET} options={{ title: t(path.RESTORE_WALLET)}} component={screen.RestoreWalletScreen} />
            <Stack.Screen name={path.PORICY} options={{ title: t(path.PORICY)}} component={screen.PoricyScreen} />
            <Stack.Screen name={path.PRIVACY} options={{ title: t(path.PRIVACY)}} component={screen.PrivacyScreen} />
        </Stack.Navigator>
    );
}

export default TopNavigator;