import React from "react";
import { StyleSheet, Image } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeNavigator from "./Home";
import TopNavigator from "./Top";
import AccountNavigator from "./Account";
import RegistFinishNavigator from "./RegistFinish";
import RestoreFinishNavigator from "./RestoreFinish";
import * as UiContext from "../../contexts/ui";
import * as Localize from "../../contexts/localize"
import * as _style from "../../const/style";
import * as screen from "../../components/pages";
import * as path from '../../const/path';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

/*
 * 画面フェード
*/
const forFade = ({ current }) => ({
    cardStyle: {
        opacity: current.progress,
    },
});

const styles = StyleSheet.create({
    icon: {
        width: 32,
        height: 32,
    },
});

/**
 * フッターのタブを作成する
 */
function TabRoutes() {
    return (
        <Tab.Navigator initialRouteName={path.HOME}
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    //console.log(route)
                    if (route.name === path.HOME) {
                        return (
                            (focused) ?
                                <Image
                                    style={styles.icon}
                                    source={require('../../../assets/images/tabbarIconHomeOn.png')}
                                />
                                :
                                <Image
                                    style={styles.icon}
                                    source={require('../../../assets/images/tabbarIconHome.png')}
                                />
                        );
                    } else if (route.name === path.ACCOUNT) {
                        return (
                            (focused) ?
                                <Image
                                    style={styles.icon}
                                    source={require('../../../assets/images/tabbarIconAccountOn.png')}
                                />
                                :
                                <Image
                                    style={styles.icon}
                                    source={require('../../../assets/images/tabbarIconAccount.png')}
                                />
                        );
                    }
                },
            })}
            //tabの色を設定する
            tabBarOptions={{
                activeTintColor: '#16a2b3', //選択時
                inactiveTintColor: 'gray',  //非選択時
            }}
        >
            <Tab.Screen name={path.HOME} component={HomeNavigator} />
            <Tab.Screen name={path.ACCOUNT} component={AccountNavigator} />
        </Tab.Navigator>
    );

}

const WalletExistsStack = createStackNavigator();
/**
 * WALLET_EXISTSの時のルーティングを定義する。
 * ・・というよりフッターを表示したくない画面を定義する時用
 */
function WalletExistsRoutes() {
    const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);

    return(
        <WalletExistsStack.Navigator initialRouteName={path.HOME} headerMode="screen" screenOptions={_style.headerstyle}>
            <WalletExistsStack.Screen name={path.HOME} component={TabRoutes} options={{title: t(path.HOME), headerShown: false}}/>
            <WalletExistsStack.Screen name={path.CAMERA} options={{ title: t(path.CAMERA)}}  component={screen.CameraScreen} />
            <WalletExistsStack.Screen name={path.SEND_COIN} options={{ title: t(path.SEND_COIN)}}  component={screen.SendCoinScreen} />
            <WalletExistsStack.Screen name={path.ETHERSCAN} options={{ title: t(path.ETHERSCAN)}}  component={screen.EtherscanScreen} />
            <WalletExistsStack.Screen name={path.RECEIVE_COIN} options={{ title: t(path.RECEIVE_COIN)}}  component={screen.ReceiveCoinScreen} />
        </WalletExistsStack.Navigator>
    );
}

/**
 * ステータスで切り分けて表示する画面オブジェクトを返す
*/
function switchingAuthStatus(status) {
    //console.log(status)
    switch (status) {
        case UiContext.Status.UN_AUTHORIZED:
        default:
            return <Stack.Screen name={path.LOADING} component={screen.LoadingScreen} />;
        case UiContext.Status.WALLET_NO_EXISTS:
            return <Stack.Screen name={path.TOP} component={TopNavigator} />;
        case UiContext.Status.WALLET_EXISTS:
            return <Stack.Screen name={path.HOME} component={WalletExistsRoutes} />; //認証された画面のみTab表示
        case UiContext.Status.REGIST_FINISH:
            return <Stack.Screen name={path.MAKE_WALLET_FINISH} component={RegistFinishNavigator} />;
        case UiContext.Status.RESTORE_FINISH:
            return <Stack.Screen name={path.PASSCODE_INPUT} component={RestoreFinishNavigator} />;
    }
}

/**
 * ルーティングを行う
*/
function AuthWithRoutes() {
    //console.log("AuthWithRoutes run..")
    const uiContext = React.useContext(UiContext.Context);
    //console.log(uiContext.applicationState)
    return (
        <Stack.Navigator initialRouteName={path.TOP} headerMode="none" screenOptions={{ cardStyleInterpolator: forFade }}>
            {switchingAuthStatus(uiContext.applicationState)}
        </Stack.Navigator>
    );
}

export default AuthWithRoutes;