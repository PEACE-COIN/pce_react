import React from "react";
import { createStackNavigator, StackCardInterpolationProps } from '@react-navigation/stack';
import { MAKE_WALLET_FINISH, PASS_PHRASE_SHOW } from "../../const/path";
import * as Localize from "../../contexts/localize"
import * as _style from "../../const/style";
import * as screen from "../../components/pages";

const ModalStack = createStackNavigator();

function RegistFinishNavigator() {
    const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);

    return (
        <ModalStack.Navigator screenOptions={_style.headerstyle}>
            <ModalStack.Screen name={MAKE_WALLET_FINISH} options={{ title: t(MAKE_WALLET_FINISH) }} component={screen.MakeWalletFinishScreen} />
            <ModalStack.Screen name={PASS_PHRASE_SHOW} options={{ title: t(PASS_PHRASE_SHOW) }} component={screen.PassPhraseShowScreen} />
        </ModalStack.Navigator>
    );
}

export default RegistFinishNavigator;