import React from "react";
import { Image, TouchableOpacity, StyleSheet, View, Modal, Text, Animated } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import * as Localize from "../../contexts/localize"
import * as _style from "../../const/style";
import { AntDesign } from '@expo/vector-icons';
import * as path from '../../const/path';
import { Dimensions } from 'react-native';

const win = Dimensions.get('window');

export default function HeaderRight(props) {
    const { t, locale, setLocale } = React.useContext(Localize.LocalizationContext);
    const { navigate } = useNavigation();
    const [modalVisible, setModalVisible] = React.useState(false);

    const { dispatch } = useNavigation();
    const onPressMenu = React.useCallback(() => {
        setModalVisible(true);
        show();
    }, [dispatch]);

    const { dispatch2 } = useNavigation();
    const onPressCamera = React.useCallback(() => {
        navigate(path.CAMERA);
    }, [dispatch2]);

    // 表示アニメーションへの参照を定義する
    const suggestionRowHeight = React.useRef(new Animated.Value(0)).current;
    //メニューの高さ
    const hei = 220;

    /**
    * メニューを表示する
    */
    const show = () => {
        suggestionRowHeight.setValue(win.height);
        // Animate the pins to shake
        Animated.timing(suggestionRowHeight, {
            toValue: win.height - hei,
            duration: 100,
            useNativeDriver: false,
        }).start();
    }
    /**
     * メニューを非表示にする
     */
    const hide = () => {
        // Animate the pins to shake
        Animated.timing(suggestionRowHeight, {
            toValue: win.height,
            duration: 100,
            useNativeDriver: false,
        }).start(() => {
            setModalVisible(false)
        });
    }

    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={onPressCamera} style={{justifyContent:"center"}}>
                <AntDesign name="camera" size={29} color="black" style={styles.icon} />
            </TouchableOpacity>
            <TouchableOpacity onPress={onPressMenu} style={{justifyContent:"center"}}>
                <Image
                    style={styles.image}
                    source={require('../../../assets/images/homeIconMore.png')}
                />
            </TouchableOpacity>
            <Modal
                animationType="none"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert('Modal has been closed.');
                }}>
                <TouchableOpacity activeOpacity={1} style={styles.centeredView} onPress={() => { hide() }}>
                    <Animated.View style={{ marginTop: suggestionRowHeight }}>
                        <TouchableOpacity activeOpacity={1} style={{}} onPress={() => {
                            navigate(path.RESTORE_WALLET)
                            hide()
                        }}>
                            <View style={styles.modalView}>
                                <Text style={{ color: '#16a2b3', textAlign: 'center' }}>{t("BTN_IMPORT_WALLET")}</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity activeOpacity={1} style={{}} onPress={() => {
                            navigate(path.MAKEWALLET)
                            hide()
                        }}>
                            <View style={styles.modalView}>
                                <Text style={{ color: '#16a2b3', textAlign: 'center' }}>{t("BTN_NEW_WALLET")}</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity activeOpacity={1} style={{}} onPress={() => { hide() }}>
                            <View style={styles.modalView}>
                                <Text style={{ color: '#16a2b3', textAlign: 'center' }}>{t("STR_CANCEL")}</Text>
                            </View>
                        </TouchableOpacity>
                    </Animated.View>
                </TouchableOpacity>
            </Modal>
        </View>
    );
}

/**
 * ローカルのスタイルシートを定義する
 */
const styles = StyleSheet.create({
    image: {
        marginRight: 10,
        marginTop: 5,
        width: 32,
        height: 32,
    },
    icon: {
        marginRight: 15,
        marginTop: 6,
        width: 32,
        height: 32,
    },
    container: {
        flex: 1,
        flexDirection: 'row',
    },
    centeredView: {
        flex: 1,
        height: win.height,
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0.1)',
    },
    modalView: {
        margin: 3,
        backgroundColor: '#fff',
        borderRadius: 20,
        padding: 20,
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        width: win.width - 20,
    },
});